<!doctype html>
<html lang="">
<head>
    <title></title>
    <base href="" />
    <link rel="canonical" href="" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" media="screen" />
    @stack('styles')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description" content="" />
    <meta name="msapplication-TileColor" content="">
    <meta name="twitter:creator" content=""/>
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <meta name="twitter:site" content=""/>
    <meta name="twitter:title" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="article:publisher" content="h"/>
    <meta property="og:description" content="" />
    <meta property="og:image" content="">
    <meta property="og:title" content="" />

    <!-- JS settings -->
    <script type="application/json" data-settings-selector="settings-json">
        {!! json_encode([
            'base_url' => url('/'),
            'api_url' => url('/api')
        ]) !!}
    </script>

</head>
<body>

<div id="app-backend"></div>
<script src="{{ asset('js/backend/main.js') }}" charset="utf-8"></script>
@stack('scripts')
</body>
</html>
