import Vue from 'vue'
//import Cookies from 'js-cookie';
import '../load-client-scripts';
import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import App from './App.vue';
import router from '@/router';
import store from '@/store';

import i18n from '../vue-i18n-config'; // Internationalization
import './global.js'

//import '@/icons' // icon
//import '@/permission' // permission control

export function createApp() {

    const app = new Vue({
        router,
        store,
        i18n,
        render: h => h(App)
    });

    return {
        app,
        router,
        store
    };

}


/**
 * Init App
 *
 * $mount allows you to explicitly mount the Vue instance when you need to.
 * This means that you can delay the mounting of your vue instance until a
 * particular element exists in your page or some async process has finished,
 * which can be particularly useful when adding vue to legacy apps which inject elements into the DOM
 */
if (document.getElementById('app-backend') !== null) {
    const { app } = createApp();
    app.$mount('#app-backend');
}

