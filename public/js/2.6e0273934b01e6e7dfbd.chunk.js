webpackJsonp([2],{

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_f759d0a6_scoped_true___ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___ = __webpack_require__(407);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_vue_vue_type_style_index_0_id_f759d0a6_rel_stylesheet_2Fscss_lang_scss_scoped_true___ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(7);






/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_f759d0a6_scoped_true___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_f759d0a6_scoped_true___["b" /* staticRenderFns */],
  false,
  null,
  "f759d0a6",
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('f759d0a6', component.options)
    } else {
      api.reload('f759d0a6', component.options)
    }
    module.hot.accept("./index.vue?vue&type=template&id=f759d0a6&scoped=true&", function () {
      api.rerender('f759d0a6', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/dashboard/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___ = __webpack_require__(408);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_TodoList__ = __webpack_require__(449);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_BoxCard__ = __webpack_require__(459);
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'DashboardAdmin',
  components: {
    TodoList: __WEBPACK_IMPORTED_MODULE_0__components_TodoList__["a" /* default */],
    BoxCard: __WEBPACK_IMPORTED_MODULE_1__components_BoxCard__["a" /* default */]
  },
  data: function data() {
    return {
      emptyGif: '/uploads/images/default.gif'
    };
  },
  created: function created() {
    if (this.$store.state.user.defaultPass) {
      console.log(this.$store.state.user.defaultPass);
      this.$notify({
        message: '您正在使用默认密码，请及时修改密码！',
        type: 'warning',
        duration: 2000,
        onClose: function onClose() {
          window.location.href = '/admin/#/user/info';
        }
      });
    }
  }
});

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___ = __webpack_require__(410);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Todo_vue__ = __webpack_require__(454);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var STORAGE_KEY = 'todos';
var filters = {
  all: function all(todos) {
    return todos;
  },
  active: function active(todos) {
    return todos.filter(function (todo) {
      return !todo.done;
    });
  },
  completed: function completed(todos) {
    return todos.filter(function (todo) {
      return todo.done;
    });
  }
};
var defalutList = [{ text: 'star this repository', done: false }, { text: 'fork this repository', done: false }, { text: 'follow author', done: false }, { text: 'vue-element-admin', done: true }, { text: 'vue', done: true }, { text: 'element-ui', done: true }, { text: 'axios', done: true }, { text: 'webpack', done: true }];
/* harmony default export */ __webpack_exports__["a"] = ({
  components: { Todo: __WEBPACK_IMPORTED_MODULE_1__Todo_vue__["a" /* default */] },
  filters: {
    pluralize: function pluralize(n, w) {
      return n === 1 ? w : w + 's';
    },
    capitalize: function capitalize(s) {
      return s.charAt(0).toUpperCase() + s.slice(1);
    }
  },
  data: function data() {
    return {
      visibility: 'all',
      filters: filters,
      // todos: JSON.parse(window.localStorage.getItem(STORAGE_KEY)) || defalutList
      todos: defalutList
    };
  },

  computed: {
    allChecked: function allChecked() {
      return this.todos.every(function (todo) {
        return todo.done;
      });
    },
    filteredTodos: function filteredTodos() {
      return filters[this.visibility](this.todos);
    },
    remaining: function remaining() {
      return this.todos.filter(function (todo) {
        return !todo.done;
      }).length;
    }
  },
  methods: {
    setLocalStorage: function setLocalStorage() {
      window.localStorage.setItem(STORAGE_KEY, __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_json_stringify___default()(this.todos));
    },
    addTodo: function addTodo(e) {
      var text = e.target.value;
      if (text.trim()) {
        this.todos.push({
          text: text,
          done: false
        });
        this.setLocalStorage();
      }
      e.target.value = '';
    },
    toggleTodo: function toggleTodo(val) {
      val.done = !val.done;
      this.setLocalStorage();
    },
    deleteTodo: function deleteTodo(todo) {
      this.todos.splice(this.todos.indexOf(todo), 1);
      this.setLocalStorage();
    },
    editTodo: function editTodo(_ref) {
      var todo = _ref.todo,
          value = _ref.value;

      todo.text = value;
      this.setLocalStorage();
    },
    clearCompleted: function clearCompleted() {
      this.todos = this.todos.filter(function (todo) {
        return !todo.done;
      });
      this.setLocalStorage();
    },
    toggleAll: function toggleAll(_ref2) {
      var _this = this;

      var done = _ref2.done;

      this.todos.forEach(function (todo) {
        todo.done = done;
        _this.setLocalStorage();
      });
    }
  }
});

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Todo_vue_vue_type_script_lang_js___ = __webpack_require__(412);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Todo_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'Todo',
  directives: {
    focus: function focus(el, _ref, _ref2) {
      var value = _ref.value;
      var context = _ref2.context;

      if (value) {
        context.$nextTick(function () {
          el.focus();
        });
      }
    }
  },
  props: {
    todo: {
      type: Object,
      default: function _default() {
        return {};
      }
    }
  },
  data: function data() {
    return {
      editing: false
    };
  },

  methods: {
    deleteTodo: function deleteTodo(todo) {
      this.$emit('deleteTodo', todo);
    },
    editTodo: function editTodo(_ref3) {
      var todo = _ref3.todo,
          value = _ref3.value;

      this.$emit('editTodo', { todo: todo, value: value });
    },
    toggleTodo: function toggleTodo(todo) {
      this.$emit('toggleTodo', todo);
    },
    doneEdit: function doneEdit(e) {
      var value = e.target.value.trim();
      var todo = this.todo;

      if (!value) {
        this.deleteTodo({
          todo: todo
        });
      } else if (this.editing) {
        this.editTodo({
          todo: todo,
          value: value
        });
        this.editing = false;
      }
    },
    cancelEdit: function cancelEdit(e) {
      e.target.value = this.todo.text;
      this.editing = false;
    }
  }
});

/***/ }),

/***/ 413:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(458);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(14)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&", function() {
		var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=scss&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_script_lang_js___ = __webpack_require__(415);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 415:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuex__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__PanThumb__ = __webpack_require__(462);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
  components: { PanThumb: __WEBPACK_IMPORTED_MODULE_2__PanThumb__["a" /* default */] },

  filters: {
    statusFilter: function statusFilter(roles) {
      var statusMap = {
        admin: '管理员',
        editor: '普通用户'
      };
      return statusMap[roles];
    }
  },
  data: function data() {
    return {
      statisticsData: {
        article_count: 1024,
        pageviews_count: 1024
      }
    };
  },

  computed: __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends___default()({}, Object(__WEBPACK_IMPORTED_MODULE_1_vuex__["b" /* mapGetters */])(['name', 'avatar', 'roles', 'siteUrl']))
});

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___ = __webpack_require__(417);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'PanThumb',
  props: {
    image: {
      type: String,
      required: true
    },
    zIndex: {
      type: Number,
      default: 1
    },
    width: {
      type: String,
      default: '150px'
    },
    height: {
      type: String,
      default: '150px'
    }
  }
});

/***/ }),

/***/ 418:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(466);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(14)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=53b566e0&scoped=true&lang=css&", function() {
		var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=53b566e0&scoped=true&lang=css&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 419:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(468);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(14)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BoxCard.vue?vue&type=style&index=0&rel=stylesheet%2Fscss&lang=scss&", function() {
		var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BoxCard.vue?vue&type=style&index=0&rel=stylesheet%2Fscss&lang=scss&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 420:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(470);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(14)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BoxCard.vue?vue&type=style&index=1&id=39519297&rel=stylesheet%2Fscss&lang=scss&scoped=true&", function() {
		var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./BoxCard.vue?vue&type=style&index=1&id=39519297&rel=stylesheet%2Fscss&lang=scss&scoped=true&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 421:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(472);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(14)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=f759d0a6&rel=stylesheet%2Fscss&lang=scss&scoped=true&", function() {
		var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&id=f759d0a6&rel=stylesheet%2Fscss&lang=scss&scoped=true&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 447:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f759d0a6_scoped_true___ = __webpack_require__(448);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f759d0a6_scoped_true___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f759d0a6_scoped_true___["b"]; });


/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "dashboard-editor-container" },
    [
      _c(
        "el-row",
        { attrs: { gutter: 8 } },
        [
          _c(
            "el-col",
            {
              staticStyle: { "margin-bottom": "30px" },
              attrs: {
                xs: { span: 24 },
                sm: { span: 12 },
                md: { span: 12 },
                lg: { span: 6, offset: 1 },
                xl: { span: 6, offset: 1 }
              }
            },
            [_c("box-card")],
            1
          ),
          _vm._v(" "),
          _c(
            "el-col",
            {
              staticStyle: { "margin-bottom": "30px" },
              attrs: {
                xs: { span: 24 },
                sm: { span: 24 },
                md: { span: 24 },
                lg: { span: 12, offset: 3 },
                xl: { span: 12, offset: 3 }
              }
            },
            [
              _c("img", {
                staticClass: "emptyGif",
                attrs: { src: _vm.emptyGif }
              })
            ]
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_7ffeccca___ = __webpack_require__(450);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___ = __webpack_require__(409);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_vue_vue_type_style_index_0_lang_scss___ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(7);






/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_7ffeccca___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_7ffeccca___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('7ffeccca', component.options)
    } else {
      api.reload('7ffeccca', component.options)
    }
    module.hot.accept("./index.vue?vue&type=template&id=7ffeccca&", function () {
      api.rerender('7ffeccca', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/dashboard/components/TodoList/index.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 450:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7ffeccca___ = __webpack_require__(451);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7ffeccca___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_7ffeccca___["b"]; });


/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", { staticClass: "todoapp" }, [
    _c("header", { staticClass: "header" }, [
      _c("input", {
        staticClass: "new-todo",
        attrs: { autocomplete: "off", placeholder: "Todo List" },
        on: {
          keyup: function($event) {
            if (
              !("button" in $event) &&
              _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
            ) {
              return null
            }
            return _vm.addTodo($event)
          }
        }
      })
    ]),
    _vm._v(" "),
    _c(
      "section",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: _vm.todos.length,
            expression: "todos.length"
          }
        ],
        staticClass: "main"
      },
      [
        _c("input", {
          staticClass: "toggle-all",
          attrs: { id: "toggle-all", type: "checkbox" },
          domProps: { checked: _vm.allChecked },
          on: {
            change: function($event) {
              _vm.toggleAll({ done: !_vm.allChecked })
            }
          }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "toggle-all" } }),
        _vm._v(" "),
        _c(
          "ul",
          { staticClass: "todo-list" },
          _vm._l(_vm.filteredTodos, function(todo, index) {
            return _c("todo", {
              key: index,
              attrs: { todo: todo },
              on: {
                toggleTodo: _vm.toggleTodo,
                editTodo: _vm.editTodo,
                deleteTodo: _vm.deleteTodo
              }
            })
          })
        )
      ]
    ),
    _vm._v(" "),
    _c(
      "footer",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: _vm.todos.length,
            expression: "todos.length"
          }
        ],
        staticClass: "footer"
      },
      [
        _c("span", { staticClass: "todo-count" }, [
          _c("strong", [_vm._v(_vm._s(_vm.remaining))]),
          _vm._v(
            "\n      " +
              _vm._s(_vm._f("pluralize")(_vm.remaining, "item")) +
              " left\n    "
          )
        ]),
        _vm._v(" "),
        _c(
          "ul",
          { staticClass: "filters" },
          _vm._l(_vm.filters, function(val, key) {
            return _c("li", { key: key }, [
              _c(
                "a",
                {
                  class: { selected: _vm.visibility === key },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      _vm.visibility = key
                    }
                  }
                },
                [_vm._v(_vm._s(_vm._f("capitalize")(key)))]
              )
            ])
          })
        )
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 452:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(453), __esModule: true };

/***/ }),

/***/ 453:
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__(2);
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Todo_vue_vue_type_template_id_21664aad___ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Todo_vue_vue_type_script_lang_js___ = __webpack_require__(411);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(7);





/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__Todo_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__Todo_vue_vue_type_template_id_21664aad___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__Todo_vue_vue_type_template_id_21664aad___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('21664aad', component.options)
    } else {
      api.reload('21664aad', component.options)
    }
    module.hot.accept("./Todo.vue?vue&type=template&id=21664aad&", function () {
      api.rerender('21664aad', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/dashboard/components/TodoList/Todo.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Todo_vue_vue_type_template_id_21664aad___ = __webpack_require__(456);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Todo_vue_vue_type_template_id_21664aad___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Todo_vue_vue_type_template_id_21664aad___["b"]; });


/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "li",
    {
      staticClass: "todo",
      class: { completed: _vm.todo.done, editing: _vm.editing }
    },
    [
      _c("div", { staticClass: "view" }, [
        _c("input", {
          staticClass: "toggle",
          attrs: { type: "checkbox" },
          domProps: { checked: _vm.todo.done },
          on: {
            change: function($event) {
              _vm.toggleTodo(_vm.todo)
            }
          }
        }),
        _vm._v(" "),
        _c("label", {
          domProps: { textContent: _vm._s(_vm.todo.text) },
          on: {
            dblclick: function($event) {
              _vm.editing = true
            }
          }
        }),
        _vm._v(" "),
        _c("button", {
          staticClass: "destroy",
          on: {
            click: function($event) {
              _vm.deleteTodo(_vm.todo)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("input", {
        directives: [
          {
            name: "focus",
            rawName: "v-focus",
            value: _vm.editing,
            expression: "editing"
          },
          {
            name: "show",
            rawName: "v-show",
            value: _vm.editing,
            expression: "editing"
          }
        ],
        staticClass: "edit",
        domProps: { value: _vm.todo.text },
        on: {
          keyup: [
            function($event) {
              if (
                !("button" in $event) &&
                _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
              ) {
                return null
              }
              return _vm.doneEdit($event)
            },
            function($event) {
              if (
                !("button" in $event) &&
                _vm._k($event.keyCode, "esc", 27, $event.key, "Escape")
              ) {
                return null
              }
              return _vm.cancelEdit($event)
            }
          ],
          blur: _vm.doneEdit
        }
      })
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_scss____default.a); 

/***/ }),

/***/ 458:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n.todoapp {\n  font: 14px 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  line-height: 1.4em;\n  color: #4d4d4d;\n  min-width: 230px;\n  max-width: 550px;\n  margin: 0 auto;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  font-weight: 300;\n  background: #fff;\n  z-index: 1;\n  position: relative;\n  /*\n  Hack to remove background from Mobile Safari.\n  Can't use it globally since it destroys checkboxes in Firefox\n*/\n}\n.todoapp button {\n    margin: 0;\n    padding: 0;\n    border: 0;\n    background: none;\n    font-size: 100%;\n    vertical-align: baseline;\n    font-family: inherit;\n    font-weight: inherit;\n    color: inherit;\n    -webkit-appearance: none;\n    appearance: none;\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n}\n.todoapp :focus {\n    outline: 0;\n}\n.todoapp .hidden {\n    display: none;\n}\n.todoapp .todoapp {\n    background: #fff;\n    margin: 130px 0 40px 0;\n    position: relative;\n    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2), 0 25px 50px 0 rgba(0, 0, 0, 0.1);\n}\n.todoapp .todoapp input::-webkit-input-placeholder {\n    font-style: italic;\n    font-weight: 300;\n    color: #e6e6e6;\n}\n.todoapp .todoapp input::-moz-placeholder {\n    font-style: italic;\n    font-weight: 300;\n    color: #e6e6e6;\n}\n.todoapp .todoapp input::input-placeholder {\n    font-style: italic;\n    font-weight: 300;\n    color: #e6e6e6;\n}\n.todoapp .todoapp h1 {\n    position: absolute;\n    top: -155px;\n    width: 100%;\n    font-size: 100px;\n    font-weight: 100;\n    text-align: center;\n    color: rgba(175, 47, 47, 0.15);\n    -webkit-text-rendering: optimizeLegibility;\n    -moz-text-rendering: optimizeLegibility;\n    text-rendering: optimizeLegibility;\n}\n.todoapp .new-todo,\n  .todoapp .edit {\n    position: relative;\n    margin: 0;\n    width: 100%;\n    font-size: 18px;\n    font-family: inherit;\n    font-weight: inherit;\n    line-height: 1.4em;\n    border: 0;\n    color: inherit;\n    padding: 6px;\n    border: 1px solid #999;\n    box-shadow: inset 0 -1px 5px 0 rgba(0, 0, 0, 0.2);\n    box-sizing: border-box;\n    -webkit-font-smoothing: antialiased;\n    -moz-osx-font-smoothing: grayscale;\n}\n.todoapp .new-todo {\n    padding: 10px 16px 16px 60px;\n    border: none;\n    background: rgba(0, 0, 0, 0.003);\n    box-shadow: inset 0 -2px 1px rgba(0, 0, 0, 0.03);\n}\n.todoapp .main {\n    position: relative;\n    z-index: 2;\n    border-top: 1px solid #e6e6e6;\n}\n.todoapp .toggle-all {\n    text-align: center;\n    border: none;\n    /* Mobile Safari */\n    opacity: 0;\n    position: absolute;\n}\n.todoapp .toggle-all + label {\n    width: 60px;\n    height: 34px;\n    font-size: 0;\n    position: absolute;\n    top: -52px;\n    left: -13px;\n    -webkit-transform: rotate(90deg);\n    transform: rotate(90deg);\n}\n.todoapp .toggle-all + label:before {\n    content: '\\276F';\n    font-size: 22px;\n    color: #e6e6e6;\n    padding: 10px 27px 10px 27px;\n}\n.todoapp .toggle-all:checked + label:before {\n    color: #737373;\n}\n.todoapp .todo-list {\n    margin: 0;\n    padding: 0;\n    list-style: none;\n}\n.todoapp .todo-list li {\n    position: relative;\n    font-size: 24px;\n    border-bottom: 1px solid #ededed;\n}\n.todoapp .todo-list li:last-child {\n    border-bottom: none;\n}\n.todoapp .todo-list li.editing {\n    border-bottom: none;\n    padding: 0;\n}\n.todoapp .todo-list li.editing .edit {\n    display: block;\n    width: 506px;\n    padding: 12px 16px;\n    margin: 0 0 0 43px;\n}\n.todoapp .todo-list li.editing .view {\n    display: none;\n}\n.todoapp .todo-list li .toggle {\n    text-align: center;\n    width: 40px;\n    /* auto, since non-WebKit browsers doesn't support input styling */\n    height: auto;\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    margin: auto 0;\n    border: none;\n    /* Mobile Safari */\n    -webkit-appearance: none;\n    appearance: none;\n}\n.todoapp .todo-list li .toggle {\n    opacity: 0;\n}\n.todoapp .todo-list li .toggle + label {\n    /*\n    Firefox requires `#` to be escaped - https://bugzilla.mozilla.org/show_bug.cgi?id=922433\n    IE and Edge requires *everything* to be escaped to render, so we do that instead of just the `#` - https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/7157459/\n  */\n    background-image: url(\"data:image/svg+xml;utf8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2240%22%20height%3D%2240%22%20viewBox%3D%22-10%20-18%20100%20135%22%3E%3Ccircle%20cx%3D%2250%22%20cy%3D%2250%22%20r%3D%2250%22%20fill%3D%22none%22%20stroke%3D%22%23ededed%22%20stroke-width%3D%223%22/%3E%3C/svg%3E\");\n    background-repeat: no-repeat;\n    background-position: center left;\n    background-size: 36px;\n}\n.todoapp .todo-list li .toggle:checked + label {\n    background-size: 36px;\n    background-image: url(\"data:image/svg+xml;utf8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2240%22%20height%3D%2240%22%20viewBox%3D%22-10%20-18%20100%20135%22%3E%3Ccircle%20cx%3D%2250%22%20cy%3D%2250%22%20r%3D%2250%22%20fill%3D%22none%22%20stroke%3D%22%23bddad5%22%20stroke-width%3D%223%22/%3E%3Cpath%20fill%3D%22%235dc2af%22%20d%3D%22M72%2025L42%2071%2027%2056l-4%204%2020%2020%2034-52z%22/%3E%3C/svg%3E\");\n}\n.todoapp .todo-list li label {\n    word-break: break-all;\n    padding: 15px 15px 15px 50px;\n    display: block;\n    line-height: 1.0;\n    font-size: 14px;\n    transition: color 0.4s;\n}\n.todoapp .todo-list li.completed label {\n    color: #d9d9d9;\n    text-decoration: line-through;\n}\n.todoapp .todo-list li .destroy {\n    display: none;\n    position: absolute;\n    top: 0;\n    right: 10px;\n    bottom: 0;\n    width: 40px;\n    height: 40px;\n    margin: auto 0;\n    font-size: 30px;\n    color: #cc9a9a;\n    transition: color 0.2s ease-out;\n    cursor: pointer;\n}\n.todoapp .todo-list li .destroy:hover {\n    color: #af5b5e;\n}\n.todoapp .todo-list li .destroy:after {\n    content: '\\D7';\n}\n.todoapp .todo-list li:hover .destroy {\n    display: block;\n}\n.todoapp .todo-list li .edit {\n    display: none;\n}\n.todoapp .todo-list li.editing:last-child {\n    margin-bottom: -1px;\n}\n.todoapp .footer {\n    color: #777;\n    position: relative;\n    padding: 10px 15px;\n    height: 40px;\n    text-align: center;\n    border-top: 1px solid #e6e6e6;\n}\n.todoapp .footer:before {\n    content: '';\n    position: absolute;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    height: 40px;\n    overflow: hidden;\n    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.2), 0 8px 0 -3px #f6f6f6, 0 9px 1px -3px rgba(0, 0, 0, 0.2), 0 16px 0 -6px #f6f6f6, 0 17px 2px -6px rgba(0, 0, 0, 0.2);\n}\n.todoapp .todo-count {\n    float: left;\n    text-align: left;\n}\n.todoapp .todo-count strong {\n    font-weight: 300;\n}\n.todoapp .filters {\n    margin: 0;\n    padding: 0;\n    position: relative;\n    z-index: 1;\n    list-style: none;\n}\n.todoapp .filters li {\n    display: inline;\n}\n.todoapp .filters li a {\n    color: inherit;\n    font-size: 12px;\n    padding: 3px 7px;\n    text-decoration: none;\n    border: 1px solid transparent;\n    border-radius: 3px;\n}\n.todoapp .filters li a:hover {\n    border-color: rgba(175, 47, 47, 0.1);\n}\n.todoapp .filters li a.selected {\n    border-color: rgba(175, 47, 47, 0.2);\n}\n.todoapp .clear-completed,\n  .todoapp html .clear-completed:active {\n    float: right;\n    position: relative;\n    line-height: 20px;\n    text-decoration: none;\n    cursor: pointer;\n}\n.todoapp .clear-completed:hover {\n    text-decoration: underline;\n}\n.todoapp .info {\n    margin: 65px auto 0;\n    color: #bfbfbf;\n    font-size: 10px;\n    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);\n    text-align: center;\n}\n.todoapp .info p {\n    line-height: 1;\n}\n.todoapp .info a {\n    color: inherit;\n    text-decoration: none;\n    font-weight: 400;\n}\n.todoapp .info a:hover {\n    text-decoration: underline;\n}\n@media screen and (-webkit-min-device-pixel-ratio: 0) {\n.todoapp .toggle-all,\n    .todoapp .todo-list li .toggle {\n      background: none;\n}\n.todoapp .todo-list li .toggle {\n      height: 40px;\n}\n}\n@media (max-width: 430px) {\n.todoapp .footer {\n      height: 50px;\n}\n.todoapp .filters {\n      bottom: 10px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__BoxCard_vue_vue_type_template_id_39519297_scoped_true___ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BoxCard_vue_vue_type_script_lang_js___ = __webpack_require__(414);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BoxCard_vue_vue_type_style_index_0_rel_stylesheet_2Fscss_lang_scss___ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__BoxCard_vue_vue_type_style_index_1_id_39519297_rel_stylesheet_2Fscss_lang_scss_scoped_true___ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(7);







/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_4__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__BoxCard_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__BoxCard_vue_vue_type_template_id_39519297_scoped_true___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__BoxCard_vue_vue_type_template_id_39519297_scoped_true___["b" /* staticRenderFns */],
  false,
  null,
  "39519297",
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('39519297', component.options)
    } else {
      api.reload('39519297', component.options)
    }
    module.hot.accept("./BoxCard.vue?vue&type=template&id=39519297&scoped=true&", function () {
      api.rerender('39519297', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/dashboard/components/BoxCard.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_template_id_39519297_scoped_true___ = __webpack_require__(461);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_template_id_39519297_scoped_true___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_template_id_39519297_scoped_true___["b"]; });


/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-card",
    {
      staticClass: "box-card-component",
      staticStyle: { "margin-left": "8px" }
    },
    [
      _c(
        "div",
        {
          staticClass: "box-card-header",
          attrs: { slot: "header" },
          slot: "header"
        },
        [_c("img", { attrs: { src: "/uploads/images/default.png" } })]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticStyle: { position: "relative" } },
        [
          _c("pan-thumb", {
            staticClass: "panThumb",
            attrs: { image: _vm.siteUrl + _vm.avatar }
          }),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "progress-item",
              staticStyle: { "padding-top": "40px" }
            },
            [
              _c("p", { staticClass: "warn-content" }, [
                _vm._v(
                  "\n        欢迎" +
                    _vm._s(_vm.name) +
                    "登陆！您是" +
                    _vm._s(_vm._f("statusFilter")(_vm.roles)) +
                    "，可以进行如下快捷操作：\n      "
                )
              ]),
              _vm._v(" "),
              _c(
                "el-row",
                { attrs: { span: 24 } },
                [
                  _c(
                    "el-col",
                    { attrs: { span: 7 } },
                    [
                      _c(
                        "router-link",
                        { attrs: { to: "/article/create" } },
                        [
                          _c(
                            "el-button",
                            {
                              staticStyle: { width: "100%" },
                              attrs: {
                                size: "mini",
                                type: "primary",
                                plain: ""
                              }
                            },
                            [_vm._v("添加文章")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 7, offset: 1 } },
                    [
                      _c(
                        "router-link",
                        { attrs: { to: "/user/info" } },
                        [
                          _c(
                            "el-button",
                            {
                              staticStyle: { width: "100%" },
                              attrs: {
                                size: "mini",
                                type: "success",
                                plain: ""
                              }
                            },
                            [_vm._v("个人中心")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    { attrs: { span: 7, offset: 1 } },
                    [
                      _c(
                        "router-link",
                        { attrs: { to: "/message/list" } },
                        [
                          _c(
                            "el-button",
                            {
                              staticStyle: { width: "100%" },
                              attrs: { size: "mini", type: "info", plain: "" }
                            },
                            [_vm._v("留言列表")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_53b566e0_scoped_true___ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___ = __webpack_require__(416);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_vue_vue_type_style_index_0_id_53b566e0_scoped_true_lang_css___ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(7);






/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_53b566e0_scoped_true___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_53b566e0_scoped_true___["b" /* staticRenderFns */],
  false,
  null,
  "53b566e0",
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('53b566e0', component.options)
    } else {
      api.reload('53b566e0', component.options)
    }
    module.hot.accept("./index.vue?vue&type=template&id=53b566e0&scoped=true&", function () {
      api.rerender('53b566e0', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/dashboard/components/PanThumb/index.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_53b566e0_scoped_true___ = __webpack_require__(464);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_53b566e0_scoped_true___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_53b566e0_scoped_true___["b"]; });


/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "pan-item",
      style: { zIndex: _vm.zIndex, height: _vm.height, width: _vm.width }
    },
    [
      _c("div", { staticClass: "pan-info" }, [
        _c(
          "div",
          { staticClass: "pan-info-roles-container" },
          [_vm._t("default")],
          2
        )
      ]),
      _vm._v(" "),
      _c("img", { staticClass: "pan-thumb", attrs: { src: _vm.image } })
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_53b566e0_scoped_true_lang_css___ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_53b566e0_scoped_true_lang_css____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_53b566e0_scoped_true_lang_css___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_53b566e0_scoped_true_lang_css____default.a); 

/***/ }),

/***/ 466:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, "\n.pan-item[data-v-53b566e0] {\n  width: 200px;\n  height: 200px;\n  border-radius: 50%;\n  display: inline-block;\n  position: relative;\n  cursor: default;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);\n}\n.pan-info-roles-container[data-v-53b566e0] {\n  padding: 20px;\n  text-align: center;\n}\n.pan-thumb[data-v-53b566e0] {\n  width: 100%;\n  height: 100%;\n  background-size: 100%;\n  border-radius: 50%;\n  overflow: hidden;\n  position: absolute;\n  transform-origin: 95% 40%;\n  transition: all 0.3s ease-in-out;\n}\n.pan-thumb[data-v-53b566e0]:after {\n  content: '';\n  width: 8px;\n  height: 8px;\n  position: absolute;\n  border-radius: 50%;\n  top: 40%;\n  left: 95%;\n  margin: -4px 0 0 -4px;\n  background: radial-gradient(ellipse at center, rgba(14, 14, 14, 1) 0%, rgba(125, 126, 125, 1) 100%);\n  box-shadow: 0 0 1px rgba(255, 255, 255, 0.9);\n}\n.pan-info[data-v-53b566e0] {\n  position: absolute;\n  width: inherit;\n  height: inherit;\n  border-radius: 50%;\n  overflow: hidden;\n  box-shadow: inset 0 0 0 5px rgba(0, 0, 0, 0.05);\n}\n.pan-info h3[data-v-53b566e0] {\n  color: #fff;\n  text-transform: uppercase;\n  position: relative;\n  letter-spacing: 2px;\n  font-size: 18px;\n  margin: 0 60px;\n  padding: 22px 0 0 0;\n  height: 85px;\n  font-family: 'Open Sans', Arial, sans-serif;\n  text-shadow: 0 0 1px #fff, 0 1px 2px rgba(0, 0, 0, 0.3);\n}\n.pan-info p[data-v-53b566e0] {\n  color: #fff;\n  padding: 10px 5px;\n  font-style: italic;\n  margin: 0 30px;\n  font-size: 12px;\n  border-top: 1px solid rgba(255, 255, 255, 0.5);\n}\n.pan-info p a[data-v-53b566e0] {\n  display: block;\n  color: #333;\n  width: 80px;\n  height: 80px;\n  background: rgba(255, 255, 255, 0.3);\n  border-radius: 50%;\n  color: #fff;\n  font-style: normal;\n  font-weight: 700;\n  text-transform: uppercase;\n  font-size: 9px;\n  letter-spacing: 1px;\n  padding-top: 24px;\n  margin: 7px auto 0;\n  font-family: 'Open Sans', Arial, sans-serif;\n  opacity: 0;\n  transition: transform 0.3s ease-in-out 0.2s, opacity 0.3s ease-in-out 0.2s, background 0.2s linear 0s;\n  transform: translateX(60px) rotate(90deg);\n}\n.pan-info p a[data-v-53b566e0]:hover {\n  background: rgba(255, 255, 255, 0.5);\n}\n.pan-item:hover .pan-thumb[data-v-53b566e0] {\n  transform: rotate(-110deg);\n}\n.pan-item:hover .pan-info p a[data-v-53b566e0] {\n  opacity: 1;\n  transform: translateX(0px) rotate(0deg);\n}\n", ""]);

// exports


/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_0_rel_stylesheet_2Fscss_lang_scss___ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_0_rel_stylesheet_2Fscss_lang_scss____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_0_rel_stylesheet_2Fscss_lang_scss___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_0_rel_stylesheet_2Fscss_lang_scss____default.a); 

/***/ }),

/***/ 468:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, ".box-card-component .el-card__header {\n  padding: 0px !important;\n}\n", ""]);

// exports


/***/ }),

/***/ 469:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_1_id_39519297_rel_stylesheet_2Fscss_lang_scss_scoped_true___ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_1_id_39519297_rel_stylesheet_2Fscss_lang_scss_scoped_true____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_1_id_39519297_rel_stylesheet_2Fscss_lang_scss_scoped_true___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_BoxCard_vue_vue_type_style_index_1_id_39519297_rel_stylesheet_2Fscss_lang_scss_scoped_true____default.a); 

/***/ }),

/***/ 470:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, ".box-card-component .box-card-header[data-v-39519297] {\n  position: relative;\n  height: 220px;\n}\n.box-card-component .box-card-header img[data-v-39519297] {\n    width: 100%;\n    height: 100%;\n    transition: all 0.2s linear;\n}\n.box-card-component .box-card-header img[data-v-39519297]:hover {\n      transform: scale(1.1, 1.1);\n      filter: contrast(130%);\n}\n.box-card-component .panThumb[data-v-39519297] {\n  z-index: 100;\n  height: 70px !important;\n  width: 70px !important;\n  position: absolute !important;\n  top: -45px;\n  left: 0px;\n  border: 5px solid #ffffff;\n  background-color: #fff;\n  margin: auto;\n  box-shadow: none !important;\n}\n.box-card-component .panThumb[data-v-39519297] .pan-info {\n    box-shadow: none !important;\n}\n.box-card-component .progress-item[data-v-39519297] {\n  margin-bottom: 10px;\n  font-size: 14px;\n}\n", ""]);

// exports


/***/ }),

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_f759d0a6_rel_stylesheet_2Fscss_lang_scss_scoped_true___ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_f759d0a6_rel_stylesheet_2Fscss_lang_scss_scoped_true____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_f759d0a6_rel_stylesheet_2Fscss_lang_scss_scoped_true___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_f759d0a6_rel_stylesheet_2Fscss_lang_scss_scoped_true____default.a); 

/***/ }),

/***/ 472:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(13)(false);
// imports


// module
exports.push([module.i, ".emptyGif[data-v-f759d0a6] {\n  display: block;\n  width: 100%;\n  margin: 0 auto;\n}\n.dashboard-editor-container[data-v-f759d0a6] {\n  min-height: 100vh;\n  padding: 50px 60px 0px;\n  background-color: #e3e3e3;\n}\n", ""]);

// exports


/***/ })

});