/**
 * Initialize Vue
 */
import Vue from 'vue'

/**
 * Locales
 */
import VueI18n from 'vue-i18n'
import Locales from './lang/vue-i18n-locales.js'

Vue.use(VueI18n);

export default new VueI18n({
    locale: (window.settings.locale)? window.settings.locale : 'en',
    messages: Locales
});
