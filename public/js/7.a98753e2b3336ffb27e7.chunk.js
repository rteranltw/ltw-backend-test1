webpackJsonp([7],{

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_39f35ca5___ = __webpack_require__(474);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__list_vue_vue_type_script_lang_js___ = __webpack_require__(449);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(3);





/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__list_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_39f35ca5___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_39f35ca5___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/Applications/MAMP/htdocs/laravel/vuejs-projects/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('39f35ca5', component.options)
    } else {
      api.reload('39f35ca5', component.options)
    }
    module.hot.accept("./list.vue?vue&type=template&id=39f35ca5&", function () {
      api.rerender('39f35ca5', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/category/list.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___ = __webpack_require__(384);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'Sticky',
  props: {
    stickyTop: {
      type: Number,
      default: 0
    },
    zIndex: {
      type: Number,
      default: 2
    },
    className: {
      type: String,
      default: ''
    }
  },
  data: function data() {
    return {
      active: false,
      position: '',
      width: undefined,
      height: undefined,
      isSticky: false
    };
  },
  mounted: function mounted() {
    this.height = this.$el.getBoundingClientRect().height;
    window.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.handleReize);
  },
  activated: function activated() {
    this.handleScroll();
  },
  destroyed: function destroyed() {
    window.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.handleReize);
  },

  methods: {
    sticky: function sticky() {
      if (this.active) {
        return;
      }
      this.position = 'fixed';
      this.active = true;
      this.width = this.width + 'px';
      this.isSticky = true;
    },
    reset: function reset() {
      if (!this.active) {
        return;
      }
      this.position = '';
      this.width = 'auto';
      this.active = false;
      this.isSticky = false;
    },
    handleScroll: function handleScroll() {
      this.width = this.$el.getBoundingClientRect().width;
      var offsetTop = this.$el.getBoundingClientRect().top;
      if (offsetTop < this.stickyTop) {
        this.sticky();
        return;
      }
      this.reset();
    },
    handleReize: function handleReize() {
      if (this.isSticky) {
        this.width = this.$el.getBoundingClientRect().width + 'px';
      }
    }
  }
});

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___ = __webpack_require__(383);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(3);





/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/Applications/MAMP/htdocs/laravel/vuejs-projects/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('cd0467c8', component.options)
    } else {
      api.reload('cd0467c8', component.options)
    }
    module.hot.accept("./index.vue?vue&type=template&id=cd0467c8&", function () {
      api.rerender('cd0467c8', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/components/Sticky/index.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___ = __webpack_require__(387);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___["b"]; });


/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { style: { height: _vm.height + "px", zIndex: _vm.zIndex } },
    [
      _c(
        "div",
        {
          class: _vm.className,
          style: {
            top: _vm.stickyTop + "px",
            zIndex: _vm.zIndex,
            position: _vm.position,
            width: _vm.width,
            height: _vm.height + "px"
          }
        },
        [_vm._t("default", [_c("div", [_vm._v("sticky")])])],
        2
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___ = __webpack_require__(450);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 450:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_category__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Sticky__ = __webpack_require__(385);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // 粘性header组件

var defaultForm = {
  name: '',
  slug: '' // 短链接
};

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'CategoryList',
  components: { Sticky: __WEBPACK_IMPORTED_MODULE_2__components_Sticky__["a" /* default */] },
  data: function data() {
    var _this = this;

    var validateRequire = function validateRequire(rule, value, callback) {
      if (value === '') {
        _this.$message({
          message: rule.field + '不能为空',
          type: 'error'
        });
        callback(new Error(rule.field + '不能为空'));
      } else {
        callback();
      }
    };
    var validateSlug = function validateSlug(rule, value, callback) {
      if (_this.isEdit === true) {
        callback();
      }
      if (value) {
        Object(__WEBPACK_IMPORTED_MODULE_1__api_category__["a" /* checkSlug */])(value).then(function (response) {
          var data = response.data;
          if (data.err) {
            return callback(new Error(data.err));
          } else {
            callback();
          }
        }).catch(function (err) {
          console.log(err);
          callback(new Error('服务错误'));
        });
      } else {
        callback(new Error('slug不能为空'));
      }
    };
    return {
      isEdit: false,
      list: null,
      total: 0,
      loading: false,
      listLoading: true,
      listQuery: {
        page: 1,
        limit: 10
      },
      centerDialogVisible: false,
      postForm: __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign___default()({}, defaultForm),
      rules: {
        name: [{ validator: validateRequire }],
        // content: [{ validator: validateRequire, trigger: 'blur' }],
        slug: [{ validator: validateSlug, trigger: 'blur' }]
      }
    };
  },
  created: function created() {
    this.getList();
  },

  methods: {
    getList: function getList() {
      var _this2 = this;

      this.listLoading = true;
      Object(__WEBPACK_IMPORTED_MODULE_1__api_category__["e" /* fetchList */])(this.listQuery).then(function (response) {
        _this2.list = response.data.items;
        // console.log(this.list)
        _this2.total = response.data.total;
        _this2.listLoading = false;
      });
    },
    handleSizeChange: function handleSizeChange(val) {
      this.listQuery.limit = val;
      this.getList();
    },
    handleCurrentChange: function handleCurrentChange(val) {
      this.listQuery.page = val;
      this.getList();
    },
    getCategory: function getCategory(val) {
      var id = val - (this.listQuery.page - 1) * this.listQuery.limit - 1;
      this.postForm = this.list[id];
      this.isEdit = true;
      this.centerDialogVisible = true;
    },
    addCategory: function addCategory() {
      this.centerDialogVisible = true;
      this.isEdit = false;
      this.postForm = __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign___default()({}, defaultForm);
      if (this.$refs.postForm !== undefined) {
        this.$refs.postForm.resetFields();
      }
    },
    editCategory: function editCategory() {
      var _this3 = this;

      this.$refs.postForm.validate(function (valid) {
        if (valid) {
          _this3.loading = true;
          if (_this3.isEdit === true) {
            Object(__WEBPACK_IMPORTED_MODULE_1__api_category__["f" /* updateCategory */])(_this3.postForm).then(function (response) {
              _this3.loading = false;
              if (response.data.err) {
                _this3.$message({
                  message: response.data.err,
                  type: 'error'
                });
              } else {
                _this3.getList();
                _this3.centerDialogVisible = false;
                _this3.$notify({
                  title: '成功',
                  message: '分类修改成功',
                  type: 'success',
                  duration: 2000
                });
              }
            }).catch(function (err) {
              console.log(err);
            });
          } else {
            Object(__WEBPACK_IMPORTED_MODULE_1__api_category__["b" /* createCategory */])(_this3.postForm).then(function (response) {
              _this3.loading = false;
              if (response.data.err) {
                _this3.$message({
                  message: response.data.err,
                  type: 'error'
                });
              } else {
                _this3.centerDialogVisible = false;
                _this3.getList();
                _this3.$notify({
                  title: '成功',
                  message: '创建分类成功',
                  type: 'success',
                  duration: 2000
                });
              }
            }).catch(function (err) {
              console.log(err);
            });
          }
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
    delCategory: function delCategory(id) {
      var _this4 = this;

      this.$confirm('此操作将永久删除该分类, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(function () {
        Object(__WEBPACK_IMPORTED_MODULE_1__api_category__["c" /* deleteCategory */])(id).then(function (response) {
          if (response.data.err) {
            _this4.$message({
              message: response.data.err,
              type: 'error'
            });
          } else {
            _this4.getList();
            _this4.$notify({
              title: '成功',
              message: '删除分类成功',
              type: 'success',
              duration: 2000
            });
          }
        });
      }).catch(function () {
        _this4.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    }
  }
});

/***/ }),

/***/ 474:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_39f35ca5___ = __webpack_require__(475);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_39f35ca5___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_39f35ca5___["b"]; });


/***/ }),

/***/ 475:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "category-list" },
    [
      _c(
        "sticky",
        { attrs: { "class-name": "sub-navbar draft" } },
        [
          _c(
            "el-button",
            {
              attrs: {
                size: "small",
                type: "primary",
                icon: "el-icon-circle-plus-outline",
                plain: ""
              },
              on: { click: _vm.addCategory }
            },
            [_vm._v("添加栏目")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "app-container" },
        [
          _c(
            "el-row",
            [
              _c(
                "el-col",
                [
                  _c(
                    "el-table",
                    {
                      directives: [
                        {
                          name: "loading",
                          rawName: "v-loading.body",
                          value: _vm.listLoading,
                          expression: "listLoading",
                          modifiers: { body: true }
                        }
                      ],
                      staticStyle: { width: "100%" },
                      attrs: {
                        data: _vm.list,
                        border: "",
                        fit: "",
                        "highlight-current-row": ""
                      }
                    },
                    [
                      _c("el-table-column", {
                        attrs: { align: "center", label: "序号", width: "50" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [_vm._v(_vm._s(scope.row.id))])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { align: "center", label: "日期", width: "200" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("parseTime")(
                                        scope.row.created_at,
                                        "{y}-{m}-{d} {h}:{i}"
                                      )
                                    )
                                  )
                                ])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "名称" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [_vm._v(_vm._s(scope.row.name))])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "短链接" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [_vm._v(_vm._s(scope.row.slug))])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "文章数量" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [
                                  _vm._v(
                                    _vm._s(
                                      scope.row.articles
                                        ? scope.row.articles.length
                                        : 0
                                    )
                                  )
                                ])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { align: "center", label: "操作", width: "100" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("el-button", {
                                  attrs: {
                                    disabled: scope.row.id < 6,
                                    type: "primary",
                                    size: "small",
                                    circle: "",
                                    icon: "el-icon-edit"
                                  },
                                  on: {
                                    click: function($event) {
                                      _vm.getCategory(scope.row.id)
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "el-tooltip",
                                  {
                                    attrs: {
                                      disabled: !scope.row.articles,
                                      effect: "dark",
                                      content:
                                        "系统分类不能删除或该分类不为空！",
                                      placement: "top"
                                    }
                                  },
                                  [
                                    _c(
                                      "span",
                                      [
                                        _c("el-button", {
                                          staticStyle: { "margin-left": "0" },
                                          attrs: {
                                            disabled:
                                              scope.row.id < 6 ||
                                              scope.row.articles,
                                            type: "danger",
                                            size: "small",
                                            icon: "el-icon-delete",
                                            circle: ""
                                          },
                                          on: {
                                            click: function($event) {
                                              _vm.delCategory(scope.row.id)
                                            }
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ]
                            }
                          }
                        ])
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "pagination-container" },
                    [
                      _c("el-pagination", {
                        attrs: {
                          "current-page": _vm.listQuery.page,
                          "page-sizes": [10, 20, 30, 50],
                          "page-size": _vm.listQuery.limit,
                          total: _vm.total,
                          background: "",
                          layout: "total, sizes, prev, pager, next, jumper"
                        },
                        on: {
                          "size-change": _vm.handleSizeChange,
                          "current-change": _vm.handleCurrentChange
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-dialog",
            {
              attrs: {
                visible: _vm.centerDialogVisible,
                title: "栏目管理",
                width: "30%",
                center: ""
              },
              on: {
                "update:visible": function($event) {
                  _vm.centerDialogVisible = $event
                }
              }
            },
            [
              _c(
                "el-form",
                {
                  ref: "postForm",
                  attrs: { model: _vm.postForm, rules: _vm.rules }
                },
                [
                  _c(
                    "el-form-item",
                    {
                      staticClass: "categoryInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "名称:",
                        prop: "name"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { placeholder: "请输入栏目名称" },
                        model: {
                          value: _vm.postForm.name,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "name", $$v)
                          },
                          expression: "postForm.name"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      staticClass: "categoryInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "短链接:",
                        prop: "slug"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { placeholder: "请输入短链接" },
                        model: {
                          value: _vm.postForm.slug,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "slug", $$v)
                          },
                          expression: "postForm.slug"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "dialog-footer",
                  attrs: { slot: "footer" },
                  slot: "footer"
                },
                [
                  _c(
                    "el-button",
                    {
                      on: {
                        click: function($event) {
                          _vm.centerDialogVisible = false
                        }
                      }
                    },
                    [_vm._v("取 消")]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: { loading: _vm.loading, type: "primary" },
                      on: { click: _vm.editCategory }
                    },
                    [_vm._v("提交")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

});