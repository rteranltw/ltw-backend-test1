webpackJsonp([9],{

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_687d7378___ = __webpack_require__(600);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__list_vue_vue_type_script_lang_js___ = __webpack_require__(524);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(8);





/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__list_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_687d7378___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_687d7378___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('687d7378', component.options)
    } else {
      api.reload('687d7378', component.options)
    }
    module.hot.accept("./list.vue?vue&type=template&id=687d7378&", function () {
      api.rerender('687d7378', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/user/list.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___ = __webpack_require__(418);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'Sticky',
  props: {
    stickyTop: {
      type: Number,
      default: 0
    },
    zIndex: {
      type: Number,
      default: 2
    },
    className: {
      type: String,
      default: ''
    }
  },
  data: function data() {
    return {
      active: false,
      position: '',
      width: undefined,
      height: undefined,
      isSticky: false
    };
  },
  mounted: function mounted() {
    this.height = this.$el.getBoundingClientRect().height;
    window.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.handleReize);
  },
  activated: function activated() {
    this.handleScroll();
  },
  destroyed: function destroyed() {
    window.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.handleReize);
  },

  methods: {
    sticky: function sticky() {
      if (this.active) {
        return;
      }
      this.position = 'fixed';
      this.active = true;
      this.width = this.width + 'px';
      this.isSticky = true;
    },
    reset: function reset() {
      if (!this.active) {
        return;
      }
      this.position = '';
      this.width = 'auto';
      this.active = false;
      this.isSticky = false;
    },
    handleScroll: function handleScroll() {
      this.width = this.$el.getBoundingClientRect().width;
      var offsetTop = this.$el.getBoundingClientRect().top;
      if (offsetTop < this.stickyTop) {
        this.sticky();
        return;
      }
      this.reset();
    },
    handleReize: function handleReize() {
      if (this.isSticky) {
        this.width = this.$el.getBoundingClientRect().width + 'px';
      }
    }
  }
});

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___ = __webpack_require__(417);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(8);





/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('cd0467c8', component.options)
    } else {
      api.reload('cd0467c8', component.options)
    }
    module.hot.accept("./index.vue?vue&type=template&id=cd0467c8&", function () {
      api.rerender('cd0467c8', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/components/Sticky/index.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___ = __webpack_require__(421);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___["b"]; });


/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { style: { height: _vm.height + "px", zIndex: _vm.zIndex } },
    [
      _c(
        "div",
        {
          class: _vm.className,
          style: {
            top: _vm.stickyTop + "px",
            zIndex: _vm.zIndex,
            position: _vm.position,
            width: _vm.width,
            height: _vm.height + "px"
          }
        },
        [_vm._t("default", [_c("div", [_vm._v("sticky")])])],
        2
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 524:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___ = __webpack_require__(525);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 525:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_user__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_Sticky__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_validate__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_vuex__ = __webpack_require__(42);


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // 粘性header组件



var defaultForm = {
  name: '',
  password: '', // 短链接
  checkPass: '',
  avatar: '',
  id: '',
  roles: { data: ['editor'] },
  email: ''
};
var roles = [{
  value: 'admin',
  label: '管理员'
}, { value: 'editor',
  label: '普通用户'
}];

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'UserList',
  components: { Sticky: __WEBPACK_IMPORTED_MODULE_3__components_Sticky__["a" /* default */] },
  filters: {
    getRoles: function getRoles(val) {
      val = val.toString();
      val = val.replace('admin', '管理员');
      val = val.replace('editor', '普通用户');
      return val;
    }
  },
  data: function data() {
    var _this = this;

    var validateRequire = function validateRequire(rule, value, callback) {
      if (value === '') {
        _this.$message({
          message: rule.field + '不能为空',
          type: 'error'
        });
        callback(new Error(rule.field + '不能为空'));
      } else {
        callback();
      }
    };
    var validatePass = function validatePass(rule, value, callback) {
      if (value) {
        console.log(value);
        if (value.length < 6) {
          callback(new Error('密码至少要6位'));
        }
        _this.$refs.postForm.validateField('checkPass');
      }
      callback();
    };
    var validatePass2 = function validatePass2(rule, value, callback) {
      if (value !== _this.postForm.password && _this.postForm.password !== '') {
        callback(new Error('两次输入密码不一致!'));
      } else {
        callback();
      }
    };
    var validateemail = function validateemail(rule, value, callback) {
      if (Object(__WEBPACK_IMPORTED_MODULE_4__utils_validate__["a" /* isvalidUsername */])(value) === false) {
        callback(new Error('请输入正确的Email'));
      } else {
        Object(__WEBPACK_IMPORTED_MODULE_2__api_user__["a" /* checkEmail */])(value).then(function (response) {
          var data = response.data;
          if (data.err) {
            callback(new Error('Email已存在'));
          }
          callback();
        }).catch(function (err) {
          console.log(err);
        });
      }
    };
    return {
      isEdit: false,
      id: null,
      list: null,
      total: 0,
      loading: false,
      listLoading: true,
      listQuery: {
        page: 1,
        limit: 10
      },
      roles: roles,
      centerDialogVisible: false,
      postForm: __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign___default()({}, defaultForm),
      rules: {
        name: [{ validator: validateRequire }],
        password: [{ validator: validatePass, trigger: 'blur' }],
        checkPass: [{ validator: validatePass2 }],
        email: [{ validator: validateemail, trigger: 'blur' }]
      }
    };
  },

  computed: __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends___default()({}, Object(__WEBPACK_IMPORTED_MODULE_5_vuex__["b" /* mapGetters */])(['siteUrl'])),
  created: function created() {
    this.getList();
    this.id = this.$store.state.user.userid;
    this.postForm.id = this.id;
  },


  methods: {
    getList: function getList() {
      var _this2 = this;

      this.listLoading = true;
      Object(__WEBPACK_IMPORTED_MODULE_2__api_user__["e" /* fetchList */])(this.listQuery).then(function (response) {
        _this2.list = response.data.items;
        // console.log(this.list)
        _this2.total = response.data.total;
        _this2.listLoading = false;
      });
    },
    addUser: function addUser() {
      this.centerDialogVisible = true;
      this.isEdit = false;
      this.postForm = __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign___default()({}, defaultForm);
      if (this.$refs.postForm !== undefined) {
        this.$refs.postForm.resetFields();
      }
    },
    getUser: function getUser(val) {
      var id = val - (this.listQuery.page - 1) * this.listQuery.limit - 1;
      this.postForm = this.list[id];
      this.isEdit = true;
      this.centerDialogVisible = true;
    },
    editUser: function editUser() {
      var _this3 = this;

      this.loading = true;
      if (this.isEdit === true) {
        Object(__WEBPACK_IMPORTED_MODULE_2__api_user__["f" /* updateUser */])(this.postForm).then(function (response) {
          _this3.loading = false;
          var data = response.data;
          if (data.err) {
            _this3.$message({
              message: data.err,
              type: 'error'
            });
          } else if (data.logout && _this3.postForm.id === _this3.$store.state.user.userid) {
            _this3.$store.dispatch('LogOut').then(function () {
              _this3.$message({
                message: data.logout,
                showClose: true,
                type: 'warning',
                onClose: function onClose() {
                  location.reload();
                }
              });
            });
          } else {
            _this3.centerDialogVisible = false;
            _this3.getList();
            _this3.$notify({
              title: '成功',
              message: '修改成功',
              type: 'success',
              duration: 2000
            });
          }
        }).catch(function (err) {
          console.log(err);
        });
      } else {
        Object(__WEBPACK_IMPORTED_MODULE_2__api_user__["b" /* createUser */])(this.postForm).then(function (response) {
          var data = response.data;
          _this3.loading = false;
          if (data.err) {
            _this3.$message({
              message: data.err,
              type: 'error'
            });
          } else {
            _this3.centerDialogVisible = false;
            _this3.getList();
            _this3.$notify({
              title: '成功',
              message: '修改成功',
              type: 'success',
              duration: 2000
            });
          }
        }).catch(function (err) {
          console.log(err);
        });
      }
    },
    delUser: function delUser(id) {
      var _this4 = this;

      this.$confirm('此操作将永久删除该用户, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(function () {
        Object(__WEBPACK_IMPORTED_MODULE_2__api_user__["c" /* deleteUser */])(id).then(function (response) {
          if (response.data.err) {
            _this4.$message({
              message: response.data.err,
              type: 'error'
            });
          } else {
            _this4.getList();
            _this4.$notify({
              title: '成功',
              message: '删除分类成功',
              type: 'success',
              duration: 2000
            });
          }
        });
      }).catch(function () {
        _this4.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    },
    handleSizeChange: function handleSizeChange(val) {
      this.listQuery.limit = val;
      this.getList();
    },
    handleCurrentChange: function handleCurrentChange(val) {
      this.listQuery.page = val;
      this.getList();
    }
  }
});

/***/ }),

/***/ 600:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_687d7378___ = __webpack_require__(601);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_687d7378___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_687d7378___["b"]; });


/***/ }),

/***/ 601:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "user-list" },
    [
      _c(
        "sticky",
        { attrs: { "class-name": "sub-navbar draft" } },
        [
          _c(
            "el-button",
            {
              attrs: {
                size: "small",
                type: "primary",
                icon: "el-icon-circle-plus-outline",
                plain: ""
              },
              on: { click: _vm.addUser }
            },
            [_vm._v("添加用户")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "app-container" },
        [
          _c(
            "el-row",
            [
              _c(
                "el-col",
                [
                  _c(
                    "el-table",
                    {
                      directives: [
                        {
                          name: "loading",
                          rawName: "v-loading.body",
                          value: _vm.listLoading,
                          expression: "listLoading",
                          modifiers: { body: true }
                        }
                      ],
                      staticStyle: { width: "100%" },
                      attrs: {
                        data: _vm.list,
                        border: "",
                        fit: "",
                        "highlight-current-row": ""
                      }
                    },
                    [
                      _c("el-table-column", {
                        attrs: { align: "center", label: "序号", width: "50" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [_vm._v(_vm._s(scope.row.id))])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "昵称", align: "center" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [_vm._v(_vm._s(scope.row.name))])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "头像", align: "center" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [
                                  _c("img", {
                                    attrs: {
                                      src: _vm.siteUrl + scope.row.avatar,
                                      width: "30"
                                    }
                                  })
                                ])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "文章", align: "center" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [
                                  _vm._v(_vm._s(scope.row.articles.length))
                                ])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { label: "权限", align: "center" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("span", [
                                  _vm._v(
                                    _vm._s(
                                      _vm._f("getRoles")(scope.row.roles.data)
                                    )
                                  )
                                ])
                              ]
                            }
                          }
                        ])
                      }),
                      _vm._v(" "),
                      _c("el-table-column", {
                        attrs: { align: "center", label: "操作", width: "100" },
                        scopedSlots: _vm._u([
                          {
                            key: "default",
                            fn: function(scope) {
                              return [
                                _c("el-button", {
                                  attrs: {
                                    type: "primary",
                                    size: "small",
                                    circle: "",
                                    icon: "el-icon-edit"
                                  },
                                  on: {
                                    click: function($event) {
                                      _vm.getUser(scope.row.id)
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "el-tooltip",
                                  {
                                    attrs: {
                                      disabled:
                                        _vm.id !== scope.row.id &&
                                        scope.row.articles.length === 0,
                                      effect: "dark",
                                      content: "不能删除自己或用户下文章不为空",
                                      placement: "top"
                                    }
                                  },
                                  [
                                    _c(
                                      "span",
                                      [
                                        _c("el-button", {
                                          staticStyle: { "margin-left": "0" },
                                          attrs: {
                                            disabled:
                                              _vm.id === scope.row.id ||
                                              scope.row.articles.length !== 0,
                                            type: "danger",
                                            size: "small",
                                            icon: "el-icon-delete",
                                            circle: ""
                                          },
                                          on: {
                                            click: function($event) {
                                              _vm.delUser(scope.row.id)
                                            }
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ]
                            }
                          }
                        ])
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "pagination-container" },
                    [
                      _c("el-pagination", {
                        attrs: {
                          "current-page": _vm.listQuery.page,
                          "page-sizes": [10, 20, 30, 50],
                          "page-size": _vm.listQuery.limit,
                          total: _vm.total,
                          background: "",
                          layout: "total, sizes, prev, pager, next, jumper"
                        },
                        on: {
                          "size-change": _vm.handleSizeChange,
                          "current-change": _vm.handleCurrentChange
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-dialog",
            {
              attrs: {
                visible: _vm.centerDialogVisible,
                title: "用户管理",
                width: "30%",
                center: ""
              },
              on: {
                "update:visible": function($event) {
                  _vm.centerDialogVisible = $event
                }
              }
            },
            [
              _c(
                "el-form",
                {
                  ref: "postForm",
                  attrs: { model: _vm.postForm, rules: _vm.rules }
                },
                [
                  _c(
                    "el-form-item",
                    {
                      staticClass: "userInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "名称:",
                        prop: "name"
                      }
                    },
                    [
                      _c("el-input", {
                        model: {
                          value: _vm.postForm.name,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "name", $$v)
                          },
                          expression: "postForm.name"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      staticClass: "userInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "邮箱:",
                        prop: "email"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { disabled: _vm.isEdit, type: "email" },
                        model: {
                          value: _vm.postForm.email,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "email", $$v)
                          },
                          expression: "postForm.email"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      staticClass: "userInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "密码:",
                        prop: "password"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { type: "password" },
                        model: {
                          value: _vm.postForm.password,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "password", $$v)
                          },
                          expression: "postForm.password"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      staticClass: "userInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "确认密码:",
                        prop: "checkPass"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { type: "password" },
                        model: {
                          value: _vm.postForm.checkPass,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "checkPass", $$v)
                          },
                          expression: "postForm.checkPass"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      staticClass: "userInfo-container-item",
                      attrs: { "label-width": "80px", label: "用户级别:" }
                    },
                    [
                      _c(
                        "el-select",
                        {
                          attrs: { multiple: "", placeholder: "请选择" },
                          model: {
                            value: _vm.postForm.roles.data,
                            callback: function($$v) {
                              _vm.$set(_vm.postForm.roles, "data", $$v)
                            },
                            expression: "postForm.roles.data"
                          }
                        },
                        _vm._l(_vm.roles, function(item) {
                          return _c("el-option", {
                            key: item.value,
                            attrs: {
                              label: item.label,
                              value: item.value ? item.value : ""
                            }
                          })
                        })
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "dialog-footer",
                  attrs: { slot: "footer" },
                  slot: "footer"
                },
                [
                  _c(
                    "el-button",
                    {
                      on: {
                        click: function($event) {
                          _vm.centerDialogVisible = false
                        }
                      }
                    },
                    [_vm._v("取 消")]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: { loading: _vm.loading, type: "primary" },
                      on: { click: _vm.editUser }
                    },
                    [_vm._v("提交")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

});