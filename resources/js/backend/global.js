import Vue from 'vue'

/* ------------------------Vue Global Config------------------------------ */
Vue.config.productionTip = false

/* ------------------------Vue Plugins------------------------------ */
import VuejsDialog from 'vuejs-dialog'
import 'vuejs-dialog/dist/vuejs-dialog.min.css'
Vue.use(VuejsDialog)

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element)

/* ------------------------Vue Global Components------------------------------ */
import DefaultLayout from './layouts/DefaultLayout/DefaultLayout.vue'
Vue.component('default-layout', DefaultLayout)

import TestDefaultLayout from './layouts/TestDefaultLayout/TestDefaultLayout.vue'
Vue.component('test-default-layout', TestDefaultLayout)

/* ------------------------Vue Global Filters------------------------------ */
import * as filters from './filters' // global filters
Object.keys(filters).forEach(key => Vue.filter(key, filters[key]))
