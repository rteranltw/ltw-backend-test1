webpackJsonp([4],{

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_0447c7ed___ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__list_vue_vue_type_script_lang_js___ = __webpack_require__(511);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__list_vue_vue_type_style_index_0_lang_css___ = __webpack_require__(550);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(1);






/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__list_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_0447c7ed___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__list_vue_vue_type_template_id_0447c7ed___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/Applications/MAMP/htdocs/laravel/vuejs-projects/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('0447c7ed', component.options)
    } else {
      api.reload('0447c7ed', component.options)
    }
    module.hot.accept("./list.vue?vue&type=template&id=0447c7ed&", function () {
      api.rerender('0447c7ed', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/field/list.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___ = __webpack_require__(430);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'Sticky',
  props: {
    stickyTop: {
      type: Number,
      default: 0
    },
    zIndex: {
      type: Number,
      default: 2
    },
    className: {
      type: String,
      default: ''
    }
  },
  data: function data() {
    return {
      active: false,
      position: '',
      width: undefined,
      height: undefined,
      isSticky: false
    };
  },
  mounted: function mounted() {
    this.height = this.$el.getBoundingClientRect().height;
    window.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.handleReize);
  },
  activated: function activated() {
    this.handleScroll();
  },
  destroyed: function destroyed() {
    window.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.handleReize);
  },

  methods: {
    sticky: function sticky() {
      if (this.active) {
        return;
      }
      this.position = 'fixed';
      this.active = true;
      this.width = this.width + 'px';
      this.isSticky = true;
    },
    reset: function reset() {
      if (!this.active) {
        return;
      }
      this.position = '';
      this.width = 'auto';
      this.active = false;
      this.isSticky = false;
    },
    handleScroll: function handleScroll() {
      this.width = this.$el.getBoundingClientRect().width;
      var offsetTop = this.$el.getBoundingClientRect().top;
      if (offsetTop < this.stickyTop) {
        this.sticky();
        return;
      }
      this.reset();
    },
    handleReize: function handleReize() {
      if (this.isSticky) {
        this.width = this.$el.getBoundingClientRect().width + 'px';
      }
    }
  }
});

/***/ }),

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___ = __webpack_require__(429);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(1);





/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_2__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__index_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__index_vue_vue_type_template_id_cd0467c8___["b" /* staticRenderFns */],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) {
  var api = require("/Applications/MAMP/htdocs/laravel/vuejs-projects/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('cd0467c8', component.options)
    } else {
      api.reload('cd0467c8', component.options)
    }
    module.hot.accept("./index.vue?vue&type=template&id=cd0467c8&", function () {
      api.rerender('cd0467c8', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/components/Sticky/index.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___ = __webpack_require__(433);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_cd0467c8___["b"]; });


/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { style: { height: _vm.height + "px", zIndex: _vm.zIndex } },
    [
      _c(
        "div",
        {
          class: _vm.className,
          style: {
            top: _vm.stickyTop + "px",
            zIndex: _vm.zIndex,
            position: _vm.position,
            width: _vm.width,
            height: _vm.height + "px"
          }
        },
        [_vm._t("default", [_c("div", [_vm._v("sticky")])])],
        2
      )
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___ = __webpack_require__(512);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_field__ = __webpack_require__(549);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_Sticky__ = __webpack_require__(431);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


 // 粘性header组件

var defaultForm = {
  name: '',
  index: ''
};

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'FieldList',
  components: { Sticky: __WEBPACK_IMPORTED_MODULE_2__components_Sticky__["a" /* default */] },
  data: function data() {
    var _this = this;

    var validateRequire = function validateRequire(rule, value, callback) {
      if (value === '') {
        _this.$message({
          message: rule.field + '不能为空',
          type: 'error'
        });
        callback(new Error(rule.field + '不能为空'));
      } else {
        callback();
      }
    };
    var validateIndex = function validateIndex(rule, value, callback) {
      if (_this.isEdit === true) {
        callback();
      }
      if (value) {
        Object(__WEBPACK_IMPORTED_MODULE_1__api_field__["a" /* checkIndex */])(value).then(function (response) {
          var data = response.data;
          if (data.err) {
            return callback(new Error(data.err));
          } else {
            callback();
          }
        }).catch(function (err) {
          console.log(err);
          callback(new Error('服务错误'));
        });
      } else {
        callback(new Error('索引不能为空'));
      }
    };
    return {
      isEdit: false,
      list: null,
      total: 0,
      loading: false,
      listLoading: true,
      multipleSelection: [],
      listQuery: {
        page: 1,
        limit: 10
      },
      fields: [],
      roles: [],
      new_status: null,
      centerDialogVisible: null,
      postForm: __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign___default()({}, defaultForm),
      rules: {
        name: [{ validator: validateRequire }],
        // content: [{ validator: validateRequire, trigger: 'blur' }],
        index: [{ validator: validateIndex, trigger: 'blur' }]
      }
    };
  },
  created: function created() {
    this.getList();
    this.roles = this.$store.state.user.roles;
  },

  methods: {
    toggleSelection: function toggleSelection() {
      this.$refs.messagesTable.clearSelection();
    },
    getField: function getField(val) {
      var id = val - (this.listQuery.page - 1) * this.listQuery.limit - 1;
      this.postForm = this.list[id];
      this.isEdit = true;
      this.centerDialogVisible = true;
    },
    editField: function editField() {
      var _this2 = this;

      this.$refs.postForm.validate(function (valid) {
        if (valid) {
          _this2.loading = true;
          console.log(_this2.postForm);
          if (_this2.isEdit === true) {
            Object(__WEBPACK_IMPORTED_MODULE_1__api_field__["e" /* updateField */])(_this2.postForm).then(function (response) {
              _this2.loading = false;
              if (response.data.err) {
                _this2.$message({
                  message: response.data.err,
                  type: 'error'
                });
              } else {
                _this2.centerDialogVisible = false;
                _this2.getList();
                _this2.$notify({
                  title: '成功',
                  message: '字段修改成功',
                  type: 'success',
                  duration: 2000
                });
              }
            }).catch(function (err) {
              console.log(err);
            });
          } else {
            Object(__WEBPACK_IMPORTED_MODULE_1__api_field__["b" /* createField */])(_this2.postForm).then(function (response) {
              _this2.loading = false;
              if (response.data.err) {
                _this2.$message({
                  message: response.data.err,
                  type: 'error'
                });
              } else {
                _this2.centerDialogVisible = false;
                _this2.getList();
                _this2.$notify({
                  title: '成功',
                  message: '创建字段成功',
                  type: 'success',
                  duration: 2000
                });
              }
            }).catch(function (err) {
              console.log(err);
            });
          }
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
    addField: function addField() {
      this.centerDialogVisible = true;
      this.isEdit = false;
      this.postForm = __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_assign___default()({}, defaultForm);
      if (this.$refs.postForm !== undefined) {
        this.$refs.postForm.resetFields();
      }
    },
    deleteSelection: function deleteSelection() {
      var _this3 = this;

      this.$confirm('此操作将永久这些删除消息, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(function () {
        var ids = _this3.multipleSelection.map(function (v) {
          return v.id;
        });
        Object(__WEBPACK_IMPORTED_MODULE_1__api_field__["c" /* deleteField */])(ids).then(function (response) {
          if (response.data.err) {
            _this3.$message({
              message: response.data.err,
              type: 'error'
            });
          } else {
            _this3.getList();
            _this3.$notify({
              title: '成功',
              type: 'success',
              duration: 2000,
              message: '删除成功!'
            });
          }
        });
      }).catch(function () {
        _this3.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    },
    tableRowClassName: function tableRowClassName(_ref) {
      var row = _ref.row,
          rowIndex = _ref.rowIndex;

      if (!row.status) {
        return 'success-row';
      }
      return '';
    },
    getFieldName: function getFieldName(val) {
      return this.fields[val];
    },
    delField: function delField(id) {
      var _this4 = this;

      this.$confirm('此操作将永久删除该分类, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(function () {
        Object(__WEBPACK_IMPORTED_MODULE_1__api_field__["c" /* deleteField */])(id).then(function (response) {
          if (response.data.err) {
            _this4.$message({
              message: response.data.err,
              type: 'error'
            });
          } else {
            _this4.getList();
            _this4.$notify({
              title: '成功',
              message: '删除分类成功',
              type: 'success',
              duration: 2000
            });
          }
        });
      }).catch(function () {
        _this4.$message({
          type: 'info',
          message: '已取消删除'
        });
      });
    },
    getList: function getList() {
      var _this5 = this;

      this.listLoading = true;
      Object(__WEBPACK_IMPORTED_MODULE_1__api_field__["d" /* fetchList */])(this.listQuery).then(function (response) {
        _this5.list = response.data.items;
        _this5.fields = response.data.fields;
        // console.log(this.list)
        _this5.total = response.data.total;
        _this5.listLoading = false;
      });
    },
    handleSizeChange: function handleSizeChange(val) {
      this.listQuery.limit = val;
      this.getList();
    },
    handleCurrentChange: function handleCurrentChange(val) {
      this.listQuery.page = val;
      this.getList();
    },
    handleSelectionChange: function handleSelectionChange(val) {
      this.multipleSelection = val;
      this.checkSelection();
    },
    checkSelection: function checkSelection() {
      if (this.multipleSelection.length === 0) {
        return false;
      }
      return true;
    }
  }
});

/***/ }),

/***/ 513:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(551);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(4)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./list.vue?vue&type=style&index=0&lang=css&", function() {
		var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./list.vue?vue&type=style&index=0&lang=css&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 547:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_0447c7ed___ = __webpack_require__(548);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_0447c7ed___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_0447c7ed___["b"]; });


/***/ }),

/***/ 548:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "message-list" },
    [
      _c(
        "sticky",
        { attrs: { "class-name": "sub-navbar draft" } },
        [
          _c(
            "el-button",
            {
              attrs: {
                size: "small",
                type: "primary",
                icon: "el-icon-circle-plus-outline",
                plain: ""
              },
              on: { click: _vm.addField }
            },
            [_vm._v("添加字段")]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "app-container" },
        [
          _c("el-row", { attrs: { gutter: 24 } }),
          _vm._v(" "),
          _c(
            "el-table",
            {
              directives: [
                {
                  name: "loading",
                  rawName: "v-loading.body",
                  value: _vm.listLoading,
                  expression: "listLoading",
                  modifiers: { body: true }
                }
              ],
              ref: "fieldsTable",
              staticStyle: { width: "100%" },
              attrs: {
                data: _vm.list,
                border: "",
                fit: "",
                "highlight-current-row": ""
              }
            },
            [
              _c("el-table-column", {
                attrs: { align: "center", label: "序号", width: "80" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [_c("span", [_vm._v(_vm._s(scope.row.id))])]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { align: "center", label: "名称" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [_c("span", [_vm._v(_vm._s(scope.row.name))])]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { align: "center", label: "索引" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [_c("span", [_vm._v(_vm._s(scope.row.index))])]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("el-table-column", {
                attrs: { align: "center", label: "操作", width: "100" },
                scopedSlots: _vm._u([
                  {
                    key: "default",
                    fn: function(scope) {
                      return [
                        _c("el-button", {
                          attrs: {
                            disabled: scope.row.id < 7,
                            circle: "",
                            type: "primary",
                            size: "small",
                            icon: "el-icon-edit"
                          },
                          on: {
                            click: function($event) {
                              _vm.getField(scope.row.id)
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("el-button", {
                          attrs: {
                            disabled: scope.row.id < 7,
                            circle: "",
                            type: "danger",
                            size: "small",
                            icon: "el-icon-delete"
                          },
                          on: {
                            click: function($event) {
                              _vm.deleteField(scope.row.id)
                            }
                          }
                        })
                      ]
                    }
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "pagination-container" },
            [
              _c("el-pagination", {
                attrs: {
                  "current-page": _vm.listQuery.page,
                  "page-sizes": [10, 20, 30, 50],
                  "page-size": _vm.listQuery.limit,
                  total: _vm.total,
                  background: "",
                  layout: "total, sizes, prev, pager, next, jumper"
                },
                on: {
                  "size-change": _vm.handleSizeChange,
                  "current-change": _vm.handleCurrentChange
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "el-dialog",
            {
              attrs: {
                visible: _vm.centerDialogVisible,
                title: "字段管理",
                width: "30%",
                center: ""
              },
              on: {
                "update:visible": function($event) {
                  _vm.centerDialogVisible = $event
                }
              }
            },
            [
              _c(
                "el-form",
                {
                  ref: "postForm",
                  attrs: { model: _vm.postForm, rules: _vm.rules }
                },
                [
                  _c(
                    "el-form-item",
                    {
                      staticClass: "fieldInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "名称:",
                        prop: "name"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { placeholder: "请输入字段名称" },
                        model: {
                          value: _vm.postForm.name,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "name", $$v)
                          },
                          expression: "postForm.name"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    {
                      staticClass: "fieldInfo-container-item",
                      attrs: {
                        "label-width": "80px",
                        label: "索引:",
                        prop: "index"
                      }
                    },
                    [
                      _c("el-input", {
                        attrs: { placeholder: "请输入字段索引" },
                        model: {
                          value: _vm.postForm.index,
                          callback: function($$v) {
                            _vm.$set(_vm.postForm, "index", $$v)
                          },
                          expression: "postForm.index"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "dialog-footer",
                  attrs: { slot: "footer" },
                  slot: "footer"
                },
                [
                  _c(
                    "el-button",
                    {
                      on: {
                        click: function($event) {
                          _vm.centerDialogVisible = false
                        }
                      }
                    },
                    [_vm._v("取 消")]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      attrs: { loading: _vm.loading, type: "primary" },
                      on: { click: _vm.editField }
                    },
                    [_vm._v("提交")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 549:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["d"] = fetchList;
/* harmony export (immutable) */ __webpack_exports__["c"] = deleteField;
/* harmony export (immutable) */ __webpack_exports__["a"] = checkIndex;
/* harmony export (immutable) */ __webpack_exports__["b"] = createField;
/* harmony export (immutable) */ __webpack_exports__["e"] = updateField;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__utils_request__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_qs__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_qs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_qs__);



function fetchList(query) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils_request__["a" /* default */])({
    url: '/field/list/',
    method: 'get',
    params: query
  });
}

function deleteField(id) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils_request__["a" /* default */])({
    url: '/field/delete/',
    method: 'post',
    data: { id: id }
  });
}

function checkIndex(index) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils_request__["a" /* default */])({
    url: '/field/check-index/',
    method: 'get',
    params: { index: index }
  });
}

function createField(field) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils_request__["a" /* default */])({
    url: '/field/create/',
    method: 'post',
    data: __WEBPACK_IMPORTED_MODULE_1_qs___default.a.stringify({ 'field': field }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  });
}

function updateField(field) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__utils_request__["a" /* default */])({
    url: '/field/update/',
    method: 'post',
    data: __WEBPACK_IMPORTED_MODULE_1_qs___default.a.stringify({ 'field': field }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  });
}

/***/ }),

/***/ 550:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_style_index_0_lang_css___ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_style_index_0_lang_css____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_style_index_0_lang_css___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_style_index_0_lang_css____default.a); 

/***/ }),

/***/ 551:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(false);
// imports


// module
exports.push([module.i, "\n.el-table .success-row {\n  background:#f0f9eb;\n}\n.demo-table-expand {\n    font-size: 0;\n}\n.demo-table-expand label {\n    width: 90px;\n    color: #99a9bf;\n}\n.demo-table-expand .el-form-item {\n    margin-right: 0;\n    margin-bottom: 0;\n    width: 50%;\n}\n", ""]);

// exports


/***/ })

});