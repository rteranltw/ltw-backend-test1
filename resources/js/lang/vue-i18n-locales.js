export default {
    "ca": {
        "groups": {
            "index": {
                "title": "Grups",
                "fields": {
                    "key": "Clau",
                    "name": "Nom",
                    "model": "Model",
                    "actions": "Accions"
                },
                "buttons": {
                    "edit": "editar",
                    "delete": "borrar"
                }
            },
            "create": {
                "title": "Crear Grup",
                "fields": {
                    "key": "Clau",
                    "name": "Nom",
                    "model": "Model",
                    "actions": "Accions"
                },
                "buttons": {
                    "create": "crear",
                    "cancel": "cancelar"
                },
                "success_msg": "Grup creada correctament!"
            },
            "edit": {
                "title": "Modificar Grup - {group}",
                "fields": {
                    "key": "Clau",
                    "name": "Nom",
                    "model": "Model",
                    "actions": "Accions"
                },
                "buttons": {
                    "save": "guardar",
                    "cancel": "cancelar"
                },
                "success_msg": "Grup modificada correctament!"
            },
            "delete": {
                "title": "Eliminar Grup - {group}",
                "info_msg": "Segur que voleu suprimir la pàgina {group}?",
                "success_msg": "Grup eliminada correctament!"
            }
        },
        "auth": {
            "failed": "Aquestes credencials no concorden amb els nostres registres.",
            "throttle": "Heu superat el nombre màxim d'intents d'accés. Per favor, torna a intentar-ho en {seconds} segons.",
            "email_address": "Correu electrònic",
            "password": "Contrasenya",
            "remember_me": "Recordar-me",
            "login": "Accedir",
            "forgot_your_password": "Has oblidat la contrasenya?",
            "register": "Registre",
            "name": "Nom",
            "confirm_password": "Confirmar contrasenya",
            "reset_password": "Restablir contrasenya",
            "send_password_reset_link": "Enviar enllaç per restablir contrasenya",
            "logout": "Tancar sessió"
        },
        "passwords": {
            "password": "Les contrasenyes han de tenir almenys sis caràcters i coincidir amb la confirmació.",
            "reset": "S'ha restablert la contrasenya.",
            "sent": "Hem enviat un correu electrònic a l'enllaç de restabliment de la vostra contrasenya.",
            "token": "Aquest token de restabliment de la contrasenya no és vàlid.",
            "user": "No podem trobar un usuari amb aquesta adreça de correu electrònic.",
            "action": "Recuperar contrasenya",
            "text": "Esteu rebent aquest correu perquè vas fer una sol·licitud de recuperació de contrasenya per a la teva compte.",
            "dismiss": "Si no vas realitzar aquesta sol·licitud, no es requereix realitzar cap altra acció."
        },
        "admin_bar": {
            "posts": "Noticies",
            "products": "Productes",
            "forms": "Formularis",
            "texts": "Textos",
            "groups": "Grups"
        },
        "pagination": {
            "previous": "&laquo; Anterior",
            "next": "Següent &raquo;",
            "first_page": "Primera pagina",
            "last_page": "Ultima pagina",
            "previous_page": "Anterior",
            "next_page": "Següent"
        },
        "commons": {
            "accept_terms": "Acepto términos y condiciones",
            "pagination": {
                "back": "Tornar",
                "prev": "Anterior",
                "next": "Següent"
            },
            "yes": "Sí",
            "no": "No",
            "actions": {
                "accept": "Acceptar",
                "cancel": "Cancelar",
                "create": "Crear",
                "edit": "Modificar",
                "delete": "Eliminar",
                "view": "Consultar",
                "send": "Enviar",
                "save": "Guardar",
                "show": "Veure"
            },
            "see_more": "Veure més",
            "see_all_fgen": "Veure totes",
            "see_all_mgen": "Ver tots",
            "info": {
                "no_search_results": "No s'han trobat resultats!"
            },
            "nav_menu": {
                "activitylogs": "Registre d'activitats",
                "address": "Adreçes",
                "create": "Crear",
                "confirmedcustomers": "Clients confirmats",
                "customers": "Clients",
                "exit": "Sortir",
                "files": "Fitxers",
                "history": "Historial",
                "home": "Web",
                "images": "Imatges",
                "image_sizes": "Mides d'imatge",
                "image_types": "Tipus d'imatge",
                "links": "Enllaços",
                "list": "Llistar",
                "map": "Mapa de clients",
                "pages": "Pàgines",
                "potentialcustomers": "Clients potencials",
                "products": "Productes",
                "redirections": "Redireccions",
                "robots": "Robots",
                "roles": "Rols",
                "texts": "Textos",
                "title": "Gestió web",
                "users": "Usuaris"
            },
            "new_gender": "{0}Nou|{1}Nova",
            "new": "Nou",
            "validation": {
                "unique": "Únic"
            }
        },
        "validation": {
            "accepted": "{attribute} ha de ser acceptat.",
            "active_url": "{attribute} no és un URL vàlid.",
            "after": "{attribute} ha de ser una data posterior a {date}.",
            "after_or_equal": "{attribute} ha de ser una data posterior o igual a {date}.",
            "alpha": "{attribute} només pot contenir lletres.",
            "alpha_dash": "{attribute} només pot contenir lletres, números i guions.",
            "alpha_num": "{attribute} només pot contenir lletres i números.",
            "array": "{attribute} ha de ser una matriu.",
            "before": "{attribute} ha de ser una data anterior a {date}.",
            "before_or_equal": "{attribute} ha de ser una data anterior o igual a {date}.",
            "between": {
                "numeric": "{attribute} ha d'estar entre {min} - {max}.",
                "file": "{attribute} ha de pesar entre {min} - {max} kilobytes.",
                "string": "{attribute} ha de tenir entre {min} - {max} caràcters.",
                "array": "{attribute} ha de tenir entre {min} - {max} ítems."
            },
            "boolean": "El camp {attribute} ha de ser verdader o fals",
            "confirmed": "La confirmació de {attribute} no coincideix.",
            "date": "{attribute} no és una data vàlida.",
            "date_format": "El camp {attribute} no concorda amb el format {format}.",
            "different": "{attribute} i {other} han de ser diferents.",
            "digits": "{attribute} ha de tenir {digits} dígits.",
            "digits_between": "{attribute} ha de tenir entre {min} i {max} dígits.",
            "dimensions": "Les dimensions de la imatge {attribute} no són vàlides.",
            "distinct": "El camp {attribute} té un valor duplicat.",
            "email": "{attribute} no és un e-mail vàlid",
            "exists": "{attribute} és invàlid.",
            "file": "El camp {attribute} ha de ser un arxiu.",
            "filled": "El camp {attribute} és obligatori.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "{attribute} ha de ser una imatge.",
            "in": "{attribute} és invàlid",
            "in_array": "El camp {attribute} no existeix dintre de {other}.",
            "integer": "{attribute} ha de ser un nombre enter.",
            "ip": "{attribute} ha de ser una adreça IP vàlida.",
            "ipv4": "{attribute} ha de ser una adreça IPv4 vàlida.",
            "ipv6": "{attribute} ha de ser una adreça IPv6 vàlida.",
            "json": "El camp {attribute} ha de ser una cadena JSON vàlida.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "{attribute} no pot ser més gran que {max}.",
                "file": "{attribute} no pot ser més gran que {max} kilobytes.",
                "string": "{attribute} no pot ser més gran que {max} caràcters.",
                "array": "{attribute} no pot tenir més de {max} ítems."
            },
            "mimes": "{attribute} ha de ser un arxiu amb format: {values}.",
            "mimetypes": "{attribute} ha de ser un arxiu amb format: {values}.",
            "min": {
                "numeric": "El tamany de {attribute} ha de ser d'almenys {min}.",
                "file": "El tamany de {attribute} ha de ser d'almenys {min} kilobytes.",
                "string": "{attribute} ha de contenir almenys {min} caràcters.",
                "array": "{attribute} ha de tenir almenys {min} ítems."
            },
            "not_in": "{attribute} és invàlid.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "{attribute} ha de ser numèric.",
            "present": "El camp {attribute} ha d'existir.",
            "regex": "El format de {attribute} és invàlid.",
            "required": "El camp {attribute} és obligatori.",
            "required_if": "El camp {attribute} és obligatori quan {other} és {value}.",
            "required_unless": "El camp {attribute} és obligatori a no ser que {other} sigui a {values}.",
            "required_with": "El camp {attribute} és obligatori quan hi ha {values}.",
            "required_with_all": "El camp {attribute} és obligatori quan hi ha {values}.",
            "required_without": "El camp {attribute} és obligatori quan no hi ha {values}.",
            "required_without_all": "El camp {attribute} és obligatori quan no hi ha cap valor dels següents: {values}.",
            "same": "{attribute} i {other} han de coincidir.",
            "size": {
                "numeric": "El tamany de {attribute} ha de ser {size}.",
                "file": "El tamany de {attribute} ha de ser {size} kilobytes.",
                "string": "{attribute} ha de contenir {size} caràcters.",
                "array": "{attribute} ha de contenir {size} ítems."
            },
            "string": "El camp {attribute} ha de ser una cadena.",
            "timezone": "El camp {attribute} ha de ser una zona vàlida.",
            "unique": "{attribute} ja està registrat i no es pot repetir.",
            "uploaded": "{attribute} ha fallat al pujar.",
            "url": "{attribute} no és una adreça web vàlida.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": {
                "name": "nom",
                "username": "usuari",
                "email": "correu electrònic",
                "first_name": "nom",
                "last_name": "cognom",
                "password": "contrasenya",
                "password_confirmation": "confirmació de la contrasenya",
                "city": "ciutat",
                "country": "país",
                "address": "adreça",
                "phone": "telèfon",
                "mobile": "mòbil",
                "age": "edat",
                "sex": "sexe",
                "gender": "gènere",
                "year": "any",
                "month": "mes",
                "day": "dia",
                "hour": "hora",
                "minute": "minut",
                "second": "segon",
                "title": "títol",
                "body": "contingut",
                "description": "descripció",
                "excerpt": "extracte",
                "date": "data",
                "time": "hora",
                "subject": "assumpte",
                "message": "missatge"
            }
        },
        "pages": {
            "index": {
                "title": "Paginas",
                "fields": {
                    "key": "Clau",
                    "title": "Titol",
                    "ht_title": "Ht titol",
                    "url": "Url",
                    "view_page": "anar a la web",
                    "actions": "Accions"
                },
                "buttons": {
                    "edit": "editar",
                    "delete": "borrar"
                }
            },
            "create": {
                "title": "Crear Pagina",
                "fields": {
                    "key": "Clau",
                    "name": "Nom",
                    "model": "Model",
                    "actions": "Accions"
                },
                "buttons": {
                    "create": "crear",
                    "cancel": "cancelar"
                },
                "success_msg": "Pagina creada correctament!"
            },
            "edit": {
                "title": "Modificar Pagina - {page}",
                "fields": {
                    "key": "Clau",
                    "name": "Nom",
                    "model": "Model",
                    "actions": "Accions"
                },
                "buttons": {
                    "save": "guardar",
                    "cancel": "cancelar"
                },
                "success_msg": "Pagina modificada correctament!"
            },
            "delete": {
                "title": "Eliminar Pagina - {page}",
                "info_msg": "Segur que voleu suprimir la pàgina {page}?",
                "success_msg": "Pagina eliminada correctament!"
            }
        }
    },
    "en": {
        "groups": {
            "index": {
                "buttons": {
                    "delete": "Delete",
                    "edit": "Edit"
                },
                "fields": {
                    "actions": "Actions",
                    "key": "Key",
                    "model": "Model",
                    "name": "Name"
                },
                "title": "Groups",
                "titles": {
                    "search_for_groups": "Search for groups"
                }
            },
            "create": {
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel"
                },
                "fields": {
                    "actions": "Actions",
                    "key": "Key",
                    "model": "Model",
                    "name": "Name"
                },
                "success_msg": "Group created successfully!",
                "title": "Create Group"
            },
            "edit": {
                "buttons": {
                    "save": "Save",
                    "cancel": "Cancel"
                },
                "fields": {
                    "actions": "Actions",
                    "key": "Key",
                    "model": "Model",
                    "name": "Name"
                },
                "success_msg": "Group modified successfully!",
                "title": "Edit Group - {group}"
            },
            "delete": {
                "info_msg": "Are you sure you want to delete the group {group} ? ",
                "success_msg": "Group deleted correctly!",
                "title": "Delete Group - {group}"
            }
        },
        "system_messages": {
            "error": {
                "common": "Error {objectid}:"
            },
            "success": {
                "common": "The {objectid} has been successfully updated"
            }
        },
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds.",
            "email_address": "E-Mail Address",
            "password": "Password",
            "remember_me": "Remember Me",
            "login": "Login",
            "forgot_your_password": "Forgot Your Password?",
            "register": "Register",
            "name": "Name",
            "confirm_password": "Confirm Password",
            "reset_password": "Reset Password",
            "send_password_reset_link": "Send Password Reset Link",
            "logout": "Logout"
        },
        "forms": {
            "index": {
                "empty": "The list is empty.",
                "fields": {
                    "contents": "Contents",
                    "created_at": "Created at"
                },
                "title": "Forms",
                "titles": {
                    "search_for_forms": "Search for forms"
                }
            }
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address.",
            "action": "Recover password",
            "text": "You are receiving this email because you made a password recovery request for your account.",
            "dismiss": "If you did not make this request, no further action is required."
        },
        "search_input": {
            "placeholder": "Write the text to search"
        },
        "locales": {
            "create": {
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel"
                },
                "fields": {
                    "default": "Default",
                    "language": "Language",
                    "locale": "Locale",
                    "location": "Location",
                    "status": "Status"
                },
                "success_msg": "Locale created successfully!",
                "title": "Create Locale"
            },
            "delete": {
                "info_msg": "Are you sure you want to delete the locale {locale} ? ",
                "success_msg": "Locale deleted correctly!",
                "title": "Delete Locale - {locale}"
            },
            "edit": {
                "buttons": {
                    "cancel": "Cancel",
                    "save": "Save"
                },
                "fields": {
                    "default": "Default",
                    "language": "Language",
                    "locale": "Locale",
                    "location": "Location",
                    "status": "Status"
                },
                "success_msg": "Locale modified successfully!",
                "title": "Edit Locale - {locale}"
            },
            "index": {
                "buttons": {
                    "create": "Create",
                    "delete": "Delete",
                    "edit": "Edit",
                    "save": "Save",
                    "save_all": "Save all"
                },
                "create": {
                    "placeholder": "Write the key to locale"
                },
                "fields": {
                    "default": "Default",
                    "language": "Language",
                    "locale": "Locale",
                    "location": "Location",
                    "status": "Status"
                },
                "links": {
                    "empty": "The list is empty. Click here to create locale."
                },
                "titles": {
                    "create_layout": "Create locale",
                    "search_for_locales": "Search for locales"
                },
                "title": "Locales"
            }
        },
        "products": {
            "index": {
                "title": "Products",
                "fields": {
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_products": "Search for products",
                    "see_languages_to_edit": "See languages to edit"
                },
                "annotations": {
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "create": "Create product",
                    "edit": "Edit",
                    "delete": "Delete"
                }
            },
            "create": {
                "title": "Create Product",
                "fields": {
                    "actions": "Actions"
                },
                "titles": {
                    "see_languages_to_create": "See languages to create"
                },
                "annotations": {
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel",
                    "discard": "Discard",
                    "save_all": "Save all",
                    "on_front_page": "On the front page"
                },
                "success_msg": "Product created successfully!"
            },
            "edit": {
                "title": "Edit Product: {product}",
                "fields": {
                    "actions": "Actions"
                },
                "titles": {
                    "see_languages_to_edit": "See languages to edit"
                },
                "annotations": {
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "delete": "Delete",
                    "delete_product": "Delete product",
                    "discard": "Discard",
                    "save_all": "Save all",
                    "on_front_page": "On the front page",
                    "see_web_page": "See web page"
                },
                "success_msg": "Product modified successfully!"
            },
            "delete": {
                "title": "Delete Product: {product}",
                "info_msg": "Are you sure you want to delete the product {product} ? ",
                "success_msg": "Product deleted correctly!"
            }
        },
        "admin_bar": {
            "posts": "Posts",
            "products": "Products",
            "forms": "Forms",
            "texts": "Texts",
            "groups": "Grupos"
        },
        "images": {
            "index": {
                "buttons": {
                    "create": "Create",
                    "edit": "Edit",
                    "delete": "Delete",
                    "save": "Save",
                    "save_all": "Save all"
                },
                "create": {
                    "placeholder": "Write the key to image"
                },
                "fields": {
                    "alt": "Alt",
                    "asociated_page": "Asociated page",
                    "file_name": "File name",
                    "image_type": "Image type",
                    "image_types": "Image types",
                    "key": "Key",
                    "pages": "Pages"
                },
                "links": {
                    "empty": "The list is empty. Click here to create image."
                },
                "title": "Images",
                "titles": {
                    "create_image": "Create image",
                    "search_for_images": "Search for images"
                }
            },
            "create": {
                "annotations": {
                    "image_dimensions": "Dimensions: {dimensions}",
                    "image_formats": "Formats: {formats}.",
                    "image_max_file_size": "Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel",
                    "select_image": "Select image",
                    "upload_new_image": "Upload new image",
                    "upload_new_image_clipping": "Upload new image clipping"
                },
                "fields": {
                    "alt": "Alt",
                    "asociated_page": "Asociated page",
                    "file_name": "File name",
                    "image_type": "Image type",
                    "image_types": "Image types",
                    "key": "Key",
                    "pages": "Pages"
                },
                "image_type": {
                    "select_default": "Select image type"
                },
                "page": {
                    "default": "Imágenes generales",
                    "select_default": "Select page"
                },
                "success_msg": "Image created successfully!",
                "title": "Create Image",
                "titles": {
                    "clipping_the_image": "Clipping the image",
                    "images": "Images"
                }
            },
            "delete": {
                "info_msg": "Are you sure you want to delete the image {image} ? ",
                "success_msg": "Image deleted correctly!",
                "title": "Delete Image - {image}"
            },
            "edit": {
                "annotations": {
                    "image_dimensions": "Dimensions: {dimensions}",
                    "image_formats": "Formats: {formats}.",
                    "image_max_file_size": "Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "cancel": "Cancel",
                    "delete_image": "Delete image",
                    "save": "Save",
                    "save_all": "Save all",
                    "select_image": "Select image",
                    "upload_new_image": "Upload new image",
                    "upload_new_image_clipping": "Upload new image clipping"
                },
                "fields": {
                    "alt": "Alt",
                    "asociated_page": "Asociated page",
                    "file_name": "File name",
                    "image_type": "Image type",
                    "image_types": "Image types",
                    "key": "Key",
                    "pages": "Pages"
                },
                "image_type": {
                    "select_default": "Select image type"
                },
                "page": {
                    "default": "Imágenes generales"
                },
                "success_msg": "Image modified successfully!",
                "title": "Edit Image - {image}",
                "titles": {
                    "clipping_the_image": "Clipping the image",
                    "images": "Images"
                }
            }
        },
        "profiles": {
            "create": {
                "annotations": {
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "cancel": "Cancel",
                    "create": "Create",
                    "discard": "Discard",
                    "save": "Save",
                    "upload_avatar": "Upload avatar"
                },
                "fields": {
                    "avatar": "Avatar",
                    "confirm_password": "Confirm Password",
                    "email": "Email",
                    "password": "Password",
                    "user_name": "User name"
                },
                "title": "Create Profile",
                "titles": [],
                "success_msg": "Profile created successfully!"
            },
            "delete": {
                "info_msg": "Are you sure you want to delete the profile {profile} ? ",
                "success_msg": "Profile deleted correctly!",
                "title": "Delete Profile: {profile}"
            },
            "edit": {
                "annotations": {
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}."
                },
                "buttons": {
                    "delete": "Delete",
                    "delete_profile": "Delete profile",
                    "discard": "Discard",
                    "save": "Save",
                    "upload_avatar": "Upload avatar"
                },
                "fields": {
                    "avatar": "Avatar",
                    "confirm_password": "Confirm Password",
                    "email": "Email",
                    "password": "Password",
                    "user_name": "User name"
                },
                "title": "Edit Profile: {profile}",
                "titles": [],
                "success_msg": "Profile modified successfully!"
            },
            "index": {
                "annotations": {
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}."
                },
                "fields": {
                    "avatar": "Avatar",
                    "confirm_password": "Confirm Password",
                    "email": "Email",
                    "password": "Password",
                    "user_name": "User name"
                },
                "title": "Profiles",
                "titles": [],
                "buttons": {
                    "create": "Create profile",
                    "delete": "Delete",
                    "edit": "Edit",
                    "upload_avatar": "Upload avatar"
                }
            }
        },
        "constants": {
            "index": {
                "title": "Constants",
                "fields": {
                    "key": "Key",
                    "value": "Value",
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_constants": "Search for constants"
                },
                "buttons": {
                    "edit": "Edit",
                    "delete": "Delete"
                }
            },
            "create": {
                "title": "Create Constant",
                "fields": {
                    "key": "Key",
                    "value": "Value",
                    "actions": "Actions"
                },
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel"
                },
                "success_msg": "Constant created successfully!"
            },
            "edit": {
                "title": "Edit Constant - {constant}",
                "fields": {
                    "key": "Key",
                    "value": "Value",
                    "actions": "Actions"
                },
                "buttons": {
                    "save": "Save",
                    "cancel": "Cancel"
                },
                "success_msg": "Constant modified successfully!"
            },
            "delete": {
                "title": "Delete Constant - {constant}",
                "info_msg": "Are you sure you want to delete the constant {constant} ? ",
                "success_msg": "Constant deleted correctly!"
            }
        },
        "texts": {
            "index": {
                "title": "Texts",
                "fields": {
                    "key": "Key",
                    "text": "Text",
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_texts": "Search for texts",
                    "see_languages_to_edit": "See languages to edit",
                    "new_text": "New text"
                },
                "buttons": {
                    "create": "Create text",
                    "edit": "Edit",
                    "delete": "Delete",
                    "list_empty_create_text": "The list is empty. Click here to create text."
                }
            },
            "search": {
                "title": "Text finder",
                "fields": {
                    "key": "Key",
                    "text": "Text",
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_texts": "Search for texts",
                    "see_languages_to_edit": "See languages to edit",
                    "new_text": "New text"
                },
                "buttons": {
                    "back_to_list": "Back to texts"
                }
            },
            "create": {
                "title": "Create Text",
                "fields": {
                    "key": "Key",
                    "pages": "Pages",
                    "translations": "Translations",
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_texts": "Search for texts",
                    "see_languages_to_edit": "See languages to create",
                    "new_text": "New text"
                },
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel",
                    "discard": "Discard",
                    "save_all": "Save all"
                },
                "success_msg": "Text created successfully!"
            },
            "edit": {
                "title": "Edit Text - {text}",
                "fields": {
                    "key": "Key",
                    "text": "Text",
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_texts": "Search for texts",
                    "see_languages_to_edit": "See languages to edit",
                    "new_text": "New text"
                },
                "buttons": {
                    "delete": "Delete",
                    "delete_text": "Delete text",
                    "create": "Create",
                    "cancel": "Cancel",
                    "discard": "Discard",
                    "save_all": "Save all"
                },
                "success_msg": "Text modified successfully!"
            },
            "delete": {
                "title": "Delete Text - {text}",
                "info_msg": "Are you sure you want to delete the text {text} ? ",
                "success_msg": "Text deleted correctly!"
            },
            "common": {
                "generic_texts": "Textos Generales",
                "generic_texts_key": "GENERIC_TEXTS"
            }
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;",
            "first_page": "First page",
            "last_page": "Last page",
            "previous_page": "Previous",
            "next_page": "Next"
        },
        "commons": {
            "accept_terms": "I accept terms and conditions",
            "pagination": {
                "back": "Back",
                "prev": "Previous",
                "next": "Next"
            },
            "yes": "Yes",
            "no": "No",
            "actions": {
                "accept": "Accept",
                "cancel": "Cancel",
                "create": "Create",
                "edit": "Edit",
                "delete": "Delete",
                "view": "View",
                "send": "Send",
                "save": "Save",
                "save_all": "Save all",
                "show": "Show"
            },
            "see_more": "See more",
            "see_all_fgen": "See all",
            "see_all_mgen": "See all",
            "info": {
                "no_search_results": "No results found!"
            },
            "nav_menu": {
                "activitylogs": "Activity logs",
                "address": "Addresses",
                "create": "Create",
                "confirmedcustomers": "Confirmed customers",
                "customers": "Customers",
                "exit": "Exit",
                "files": "Files",
                "history": "History",
                "home": "Web",
                "images": "Images",
                "image_sizes": "Image sizes",
                "image_types": "Image types",
                "links": "Links",
                "list": "List",
                "map": "Customer map",
                "pages": "Pages",
                "potentialcustomers": "Potential customers",
                "products": "Products",
                "redirections": "Redirections",
                "robots": "Robots",
                "roles": "Roles",
                "texts": "Texts",
                "title": "Web management",
                "users": "Users"
            },
            "new_gender": "{0}New|{1}New",
            "new": "New",
            "validation": {
                "unique": "Unique"
            }
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, dashes and underscores.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        },
        "posts": {
            "create": {
                "annotations": {
                    "allowed_sources": "Allowed sources: {sources}.",
                    "file_formats": "Formats: {formats}.",
                    "file_max_file_size": "Maximum file size: {max_file_size}.",
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}.",
                    "maximum_of_words": "Maximum of words: {max_words}"
                },
                "buttons": {
                    "cancel": "Cancel",
                    "create": "Create",
                    "discard": "Discard",
                    "manage_categories": "Manage categories",
                    "manage_tags": "Manage tags",
                    "on_front_page": "On the front page",
                    "save_all": "Save all",
                    "upload_images": "Upload images",
                    "upload_files": "Upload files"
                },
                "fields": {
                    "abstract_text": "Abstract text",
                    "actions": "Actions",
                    "categories": "Categories",
                    "description": "Description",
                    "files": {
                        "file_name": "File name",
                        "name": "Name",
                        "title": "Title"
                    },
                    "images": {
                        "alt": "Alt",
                        "file_name": "File name"
                    },
                    "key": "Key",
                    "name": "Internal name",
                    "post_text": "Text",
                    "post_title": "Title of the news; H1",
                    "publication_day": "Schedule publication day",
                    "publication_time": "Schedule publication time",
                    "tags": "Tags",
                    "title": "Title",
                    "url": "Url",
                    "video_url": "Url",
                    "view": "Associated view"
                },
                "title": "Create Post",
                "titles": {
                    "attached_files": "Attached files",
                    "categories_and_tags": "Categories ans tags",
                    "gallery": "Gallery",
                    "highlight_in": "Highlight in",
                    "images": "Images",
                    "publish_in": "Publish in",
                    "schedule_publication_day_and_hour": "Schedule publication day and hour",
                    "see_languages_to_create": "See languages to create",
                    "seo_keys": "SEO keys",
                    "texts": "Texts",
                    "video": "Video"
                },
                "success_msg": "Post created successfully!"
            },
            "delete": {
                "info_msg": "Are you sure you want to delete the post {post} ? ",
                "success_msg": "Post deleted correctly!",
                "title": "Delete Post: {post}"
            },
            "edit": {
                "annotations": {
                    "allowed_sources": "Allowed sources: {sources}.",
                    "file_formats": "Formats: {formats}.",
                    "file_max_file_size": "Maximum file size: {max_file_size}.",
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}.",
                    "maximum_of_words": "Maximum of words: {max_words}"
                },
                "buttons": {
                    "delete": "Delete",
                    "delete_post": "Delete post",
                    "discard": "Discard",
                    "manage_categories": "Manage categories",
                    "manage_tags": "Manage tags",
                    "on_front_page": "On the front page",
                    "save_all": "Save all",
                    "see_in_web_page": "See in web page",
                    "upload_images": "Upload images",
                    "upload_files": "Upload files"
                },
                "fields": {
                    "abstract_text": "Abstract text",
                    "actions": "Actions",
                    "categories": "Categories",
                    "description": "Description",
                    "files": {
                        "file_name": "File name",
                        "name": "Name",
                        "title": "Title"
                    },
                    "images": {
                        "alt": "Alt",
                        "file_name": "File name"
                    },
                    "key": "Key",
                    "name": "Internal name",
                    "post_text": "Text",
                    "post_title": "Title of the news; H1 *",
                    "publication_day": "Schedule publication day",
                    "publication_time": "Schedule publication time",
                    "tags": "Tags",
                    "title": "Title",
                    "url": "Url",
                    "video_url": "Url",
                    "view": "Associated view"
                },
                "title": "Edit Post: {post}",
                "titles": {
                    "attached_files": "Attached files",
                    "categories_and_tags": "Categories ans tags",
                    "gallery": "Gallery",
                    "highlight_in": "Highlight in",
                    "images": "Images",
                    "publish_in": "Publish in",
                    "schedule_publication_day_and_hour": "Schedule publication day and hour",
                    "see_languages_to_edit": "See languages to edit",
                    "seo_keys": "SEO keys",
                    "texts": "Texts",
                    "video": "Video"
                },
                "success_msg": "Post modified successfully!"
            },
            "index": {
                "annotations": {
                    "allowed_sources": "Allowed sources: {sources}.",
                    "file_formats": "Formats: {formats}.",
                    "file_max_file_size": "Maximum file size: {max_file_size}.",
                    "image_specifications": "Dimensions: {dimensions} . Formats: {formats} . Maximum file size: {max_file_size}.",
                    "maximum_of_words": "Maximum of words: {max_words}"
                },
                "buttons": {
                    "create": "Create post",
                    "edit": "Edit",
                    "delete": "Delete",
                    "manage_categories": "Manage categories",
                    "manage_tags": "Manage tags",
                    "upload_images": "Upload images",
                    "upload_files": "Upload files"
                },
                "fields": {
                    "abstract_text": "Abstract text",
                    "actions": "Actions",
                    "categories": "Categories",
                    "description": "Description",
                    "files": {
                        "file_name": "File name",
                        "name": "Name",
                        "title": "Title"
                    },
                    "images": {
                        "alt": "Alt",
                        "file_name": "File name"
                    },
                    "key": "Key",
                    "name": "Internal name",
                    "post_text": "Text",
                    "post_title": "Title of the news; H1 *",
                    "publication_day": "Schedule publication day",
                    "publication_time": "Schedule publication time",
                    "tags": "Tags",
                    "title": "Title",
                    "url": "Url",
                    "video_url": "Url",
                    "view": "Associated view"
                },
                "title": "Posts",
                "titles": {
                    "attached_files": "Attached files",
                    "categories_and_tags": "Categories ans tags",
                    "gallery": "Gallery",
                    "schedule_publication_day_and_hour": "Schedule publication day and hour",
                    "search_for_posts": "Search for posts",
                    "see_languages_to_edit": "See languages to edit",
                    "seo_keys": "SEO keys",
                    "texts": "Texts",
                    "video": "Video"
                }
            }
        },
        "categories": {
            "index": {
                "title": "Categories",
                "fields": [],
                "titles": {
                    "search_for_categories": "Search for categories"
                },
                "buttons": {
                    "create": "Create",
                    "edit": "Edit",
                    "delete": "Delete"
                }
            },
            "create": {
                "title": "Create Category",
                "fields": [],
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel"
                },
                "success_msg": "Category created successfully!"
            },
            "edit": {
                "title": "Edit Category - {category}",
                "fields": [],
                "buttons": {
                    "save": "Save",
                    "cancel": "Cancel"
                },
                "success_msg": "Category modified successfully!"
            },
            "delete": {
                "title": "Delete Category - {category}",
                "info_msg": "Are you sure you want to delete the category {category} ? ",
                "success_msg": "Category deleted correctly!"
            }
        },
        "pages": {
            "index": {
                "title": "Pages",
                "fields": {
                    "key": "Key",
                    "name": "Internal name",
                    "view": "Associated view",
                    "url": "Url",
                    "title": "Title",
                    "description": "Description",
                    "texts": {
                        "text": "Text"
                    },
                    "images": {
                        "alt": "Alt",
                        "file_name": "File name"
                    },
                    "actions": "Actions"
                },
                "titles": {
                    "search_for_pages": "Search for pages",
                    "see_languages_to_edit": "See languages to edit",
                    "seo_keys": "SEO keys",
                    "texts": "Texts",
                    "new_text": "New text",
                    "images": "Imágenes"
                },
                "buttons": {
                    "create": "Create page",
                    "edit": "Edit",
                    "delete": "Delete",
                    "list_empty_create_page": "The list is empty. Click here to create page."
                }
            },
            "create": {
                "title": "Create Page",
                "fields": {
                    "key": "Key",
                    "name": "Internal name",
                    "view": "Associated view",
                    "url": "Url",
                    "title": "Title",
                    "description": "Description",
                    "texts": {
                        "text": "Text"
                    },
                    "images": {
                        "alt": "Alt",
                        "file_name": "File name"
                    },
                    "actions": "Actions"
                },
                "titles": {
                    "see_languages_to_create": "See languages to create",
                    "seo_keys": "SEO keys",
                    "texts": "Texts",
                    "new_text": "New text",
                    "images": "Imágenes"
                },
                "buttons": {
                    "create": "Create",
                    "cancel": "Cancel",
                    "discard": "Discard",
                    "save_all": "Save all",
                    "see_in_web_page": "See in web page",
                    "upload_new_image": "Upload new image"
                },
                "success_msg": "Page created successfully!"
            },
            "edit": {
                "title": "Edit Page - {page}",
                "fields": {
                    "key": "Key",
                    "name": "Internal name",
                    "view": "Associated view",
                    "url": "Url",
                    "title": "Title",
                    "description": "Description",
                    "texts": {
                        "text": "Text"
                    },
                    "images": {
                        "alt": "Alt",
                        "file_name": "File name"
                    },
                    "actions": "Actions"
                },
                "titles": {
                    "see_languages_to_edit": "See languages to edit",
                    "seo_keys": "SEO keys",
                    "texts": "Texts",
                    "new_text": "New text",
                    "images": "Imágenes"
                },
                "buttons": {
                    "delete": "Delete",
                    "delete_page": "Delete page",
                    "create": "Create",
                    "cancel": "Cancel",
                    "discard": "Discard",
                    "save_all": "Save all",
                    "see_in_web_page": "See in web page",
                    "upload_new_image": "Upload new image"
                },
                "success_msg": "Page modified successfully!"
            },
            "delete": {
                "title": "Delete Page - {page}",
                "info_msg": "Are you sure you want to delete the page {page} ? ",
                "success_msg": "Page deleted correctly!"
            }
        },
        "layouts": {
            "create": {
                "buttons": {
                    "cancel": "Cancel",
                    "create": "Create"
                },
                "fields": {
                    "actions": "Actions",
                    "controller_method": "Controller@Method",
                    "key": "Key",
                    "view": "View"
                },
                "success_msg": "Layout created successfully!",
                "title": "Create Layout"
            },
            "delete": {
                "info_msg": "Are you sure you want to delete the layout {layout} ? ",
                "success_msg": "Layout deleted correctly!",
                "title": "Delete Layout - {layout}"
            },
            "edit": {
                "buttons": {
                    "cancel": "Cancel",
                    "save": "Save"
                },
                "fields": {
                    "actions": "Actions",
                    "controller_method": "Controller@Method",
                    "key": "Key",
                    "view": "View"
                },
                "success_msg": "Layout modified successfully!",
                "title": "Edit Layout - {layout}"
            },
            "index": {
                "buttons": {
                    "create": "Create",
                    "delete": "Delete",
                    "edit": "Edit",
                    "save": "Save",
                    "save_all": "Save all"
                },
                "create": {
                    "placeholder": "Write the key to layout"
                },
                "fields": {
                    "actions": "Actions",
                    "controller_method": "Controller@Method",
                    "key": "Key",
                    "view": "View"
                },
                "links": {
                    "empty": "The list is empty. Click here to create layout."
                },
                "title": "Layouts",
                "titles": {
                    "create_layout": "Create layout",
                    "search_for_layouts": "Search for layouts"
                }
            }
        }
    },
    "es": {
        "groups": {
            "index": {
                "title": "Grupos",
                "fields": {
                    "key": "Clave",
                    "name": "Nombre",
                    "model": "Modelo",
                    "actions": "Acciones"
                },
                "buttons": {
                    "edit": "editar",
                    "delete": "borrar"
                }
            },
            "create": {
                "title": "Crear Grupo",
                "fields": {
                    "key": "Clave",
                    "name": "Nombre",
                    "model": "Modelo",
                    "actions": "Acciones"
                },
                "buttons": {
                    "create": "crear",
                    "cancel": "cancelar"
                },
                "success_msg": "Grup creado correctament!"
            },
            "edit": {
                "title": "Modificar Grupo - {group}",
                "fields": {
                    "key": "Clave",
                    "name": "Nombre",
                    "model": "Modelo",
                    "actions": "Acciones"
                },
                "buttons": {
                    "save": "guardar",
                    "cancel": "cancelar"
                },
                "success_msg": "¡Grupo modificado correctamente!"
            },
            "delete": {
                "title": "Eliminar Grupo - {group}",
                "info_msg": "¿Estás seguro de que deseas eliminar la grupo {group} ?",
                "success_msg": "¡Grupo modificado correctamente!"
            }
        },
        "auth": {
            "failed": "Las credenciales introducidas son incorrectas.",
            "throttle": "Demasiados intentos de inicio de sesión. Por favor, inténtelo de Crear en {segundos} segundos.",
            "email_address": "Correo electrónico",
            "password": "Contraseña",
            "remember_me": "Recordarme",
            "login": "Acceder",
            "forgot_your_password": "¿Olvidaste tu contraseña?",
            "register": "Registro",
            "name": "Nombre",
            "confirm_password": "Confirmar contraseña",
            "reset_password": "Restablecer contraseña",
            "send_password_reset_link": "Enviar enlace para restablecer contraseña",
            "logout": "Cerrar sesión"
        },
        "passwords": {
            "password": "Las contraseñas deben tener al menos 6 carácteres y coincidir.",
            "reset": "¡Tu contraseña ha sido restablecida!",
            "sent": "¡Le hemos enviado por correo electrónico el enlace de restablecimiento de contraseña!",
            "token": "Este token de restablecimiento de contraseña no es válido.",
            "user": "No podemos encontrar un usuario con esa dirección de correo electrónico.",
            "action": "Recuperar contraseña",
            "text": "Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.",
            "dismiss": "Si no realizaste esta solicitud, no se requiere realizar ninguna otra acción."
        },
        "admin_bar": {
            "posts": "Noticias",
            "products": "Productos",
            "forms": "Formularios",
            "texts": "Textos",
            "groups": "Groups",
            "layouts": "Layouts"
        },
        "pagination": {
            "previous": "&laquo; Anterior",
            "next": "Siguiente &raquo;",
            "first_page": "Primera pagina",
            "last_page": "Ultima pagina",
            "previous_page": "Anterior",
            "next_page": "Siguiente"
        },
        "commons": {
            "accept_terms": "Acepto términos y condiciones",
            "pagination": {
                "back": "Volver",
                "prev": "Anterior",
                "next": "Siguiente"
            },
            "yes": "Sí",
            "no": "No",
            "actions": {
                "accept": "Aceptar",
                "cancel": "Cancelar",
                "create": "Crear",
                "edit": "Modificar",
                "delete": "Eliminar",
                "view": "Consultar",
                "send": "Enviar",
                "save": "Guardar",
                "show": "Ver"
            },
            "see_more": "Ver más",
            "see_all_fgen": "Ver todas",
            "see_all_mgen": "Ver todos",
            "info": {
                "no_search_results": "¡No se han encontrado resultados!"
            },
            "nav_menu": {
                "activitylogs": "Registro de actividades",
                "address": "Direcciones",
                "create": "Crear",
                "confirmedcustomers": "Clientes confirmados",
                "customers": "Clientes",
                "exit": "Salir",
                "files": "Ficheros",
                "history": "Historial",
                "home": "Web",
                "images": "Imágenes",
                "image_sizes": "Tamaños de imagen",
                "image_types": "Tipos de imagen",
                "links": "Enlaces",
                "list": "Listas",
                "map": "Mapa de clientes",
                "pages": "Páginas",
                "potentialcustomers": "Clientes potenciales",
                "products": "Productos",
                "redirections": "Redirecciones",
                "robots": "Robots",
                "roles": "Roles",
                "texts": "Textos",
                "title": "Gestión web",
                "users": "Usuarios"
            },
            "new_gender": "{0}Nuevo|{1}Nueva",
            "new": "Nuevo",
            "validation": {
                "unique": "Único"
            }
        },
        "validation": {
            "accepted": "{attribute} debe ser aceptado.",
            "active_url": "{attribute} no es una URL válida.",
            "after": "{attribute} debe ser una fecha posterior a {date}.",
            "after_or_equal": "{attribute} debe ser una fecha posterior o igual a {date}.",
            "alpha": "{attribute} sólo debe contener letras.",
            "alpha_dash": "{attribute} sólo debe contener letras, números y guiones.",
            "alpha_num": "{attribute} sólo debe contener letras y números.",
            "array": "{attribute} debe ser un conjunto.",
            "before": "{attribute} debe ser una fecha anterior a {date}.",
            "before_or_equal": "{attribute} debe ser una fecha anterior o igual a {date}.",
            "between": {
                "numeric": "{attribute} tiene que estar entre {min} - {max}.",
                "file": "{attribute} debe pesar entre {min} - {max} kilobytes.",
                "string": "{attribute} tiene que tener entre {min} - {max} caracteres.",
                "array": "{attribute} tiene que tener entre {min} - {max} ítems."
            },
            "boolean": "El campo {attribute} debe tener un valor verdadero o falso.",
            "confirmed": "La confirmación de {attribute} no coincide.",
            "date": "{attribute} no es una fecha válida.",
            "date_format": "{attribute} no corresponde al formato {format}.",
            "different": "{attribute} y {other} deben ser diferentes.",
            "digits": "{attribute} debe tener {digits} dígitos.",
            "digits_between": "{attribute} debe tener entre {min} y {max} dígitos.",
            "dimensions": "Las dimensiones de la imagen {attribute} no son válidas.",
            "distinct": "El campo {attribute} contiene un valor duplicado.",
            "email": "{attribute} no es un correo válido",
            "exists": "{attribute} es inválido.",
            "file": "El campo {attribute} debe ser un archivo.",
            "filled": "El campo {attribute} es obligatorio.",
            "gt": {
                "numeric": "El campo {attribute} debe ser mayor que {value}.",
                "file": "El campo {attribute} debe tener más de {value} kilobytes.",
                "string": "El campo {attribute} debe tener más de {value} caracteres.",
                "array": "El campo {attribute} debe tener más de {value} elementos."
            },
            "gte": {
                "numeric": "El campo {attribute} debe ser como mínimo {value}.",
                "file": "El campo {attribute} debe tener como mínimo {value} kilobytes.",
                "string": "El campo {attribute} debe tener como mínimo {value} caracteres.",
                "array": "El campo {attribute} debe tener como mínimo {value} elementos."
            },
            "image": "{attribute} debe ser una imagen.",
            "in": "{attribute} es inválido.",
            "in_array": "El campo {attribute} no existe en {other}.",
            "integer": "{attribute} debe ser un número entero.",
            "ip": "{attribute} debe ser una dirección IP válida.",
            "ipv4": "{attribute} debe ser un dirección IPv4 válida",
            "ipv6": "{attribute} debe ser un dirección IPv6 válida.",
            "json": "El campo {attribute} debe tener una cadena JSON válida.",
            "lt": {
                "numeric": "El campo {attribute} debe ser menor que {value}.",
                "file": "El campo {attribute} debe tener menos de {value} kilobytes.",
                "string": "El campo {attribute} debe tener menos de {value} caracteres.",
                "array": "El campo {attribute} debe tener menos de {value} elementos."
            },
            "lte": {
                "numeric": "El campo {attribute} debe ser como máximo {value}.",
                "file": "El campo {attribute} debe tener como máximo {value} kilobytes.",
                "string": "El campo {attribute} debe tener como máximo {value} caracteres.",
                "array": "El campo {attribute} debe tener como máximo {value} elementos."
            },
            "max": {
                "numeric": "{attribute} no debe ser mayor a {max}.",
                "file": "{attribute} no debe ser mayor que {max} kilobytes.",
                "string": "{attribute} no debe ser mayor que {max} caracteres.",
                "array": "{attribute} no debe tener más de {max} elementos."
            },
            "mimes": "{attribute} debe ser un archivo con formato: {values}.",
            "mimetypes": "{attribute} debe ser un archivo con formato: {values}.",
            "min": {
                "numeric": "El tamaño de {attribute} debe ser de al menos {min}.",
                "file": "El tamaño de {attribute} debe ser de al menos {min} kilobytes.",
                "string": "{attribute} debe contener al menos {min} caracteres.",
                "array": "{attribute} debe tener al menos {min} elementos."
            },
            "not_in": "{attribute} es inválido.",
            "not_regex": "El formato del campo {attribute} no es válido.",
            "numeric": "{attribute} debe ser numérico.",
            "present": "El campo {attribute} debe estar presente.",
            "regex": "El formato de {attribute} es inválido.",
            "required": "El campo {attribute} es obligatorio.",
            "required_if": "El campo {attribute} es obligatorio cuando {other} es {value}.",
            "required_unless": "El campo {attribute} es obligatorio a menos que {other} esté en {values}.",
            "required_with": "El campo {attribute} es obligatorio cuando {values} está presente.",
            "required_with_all": "El campo {attribute} es obligatorio cuando {values} está presente.",
            "required_without": "El campo {attribute} es obligatorio cuando {values} no está presente.",
            "required_without_all": "El campo {attribute} es obligatorio cuando ninguno de {values} estén presentes.",
            "same": "{attribute} y {other} deben coincidir.",
            "size": {
                "numeric": "El tamaño de {attribute} debe ser {size}.",
                "file": "El tamaño de {attribute} debe ser {size} kilobytes.",
                "string": "{attribute} debe contener {size} caracteres.",
                "array": "{attribute} debe contener {size} elementos."
            },
            "string": "El campo {attribute} debe ser una cadena de caracteres.",
            "timezone": "El {attribute} debe ser una zona válida.",
            "unique": "{attribute} ya ha sido registrado.",
            "uploaded": "Subir {attribute} ha fallado.",
            "url": "El formato {attribute} es inválido.",
            "custom": {
                "password": {
                    "min": "La {attribute} debe contener más de {min} caracteres"
                },
                "email": {
                    "unique": "El {attribute} ya ha sido registrado."
                }
            },
            "attributes": {
                "name": "nombre",
                "username": "usuario",
                "email": "correo electrónico",
                "first_name": "nombre",
                "last_name": "apellido",
                "password": "contraseña",
                "password_confirmation": "confirmación de la contraseña",
                "city": "ciudad",
                "country": "país",
                "address": "dirección",
                "phone": "teléfono",
                "mobile": "móvil",
                "age": "edad",
                "sex": "sexo",
                "gender": "género",
                "year": "año",
                "month": "mes",
                "day": "día",
                "hour": "hora",
                "minute": "minuto",
                "second": "segundo",
                "title": "título",
                "content": "contenido",
                "body": "contenido",
                "description": "descripción",
                "excerpt": "extracto",
                "date": "fecha",
                "time": "hora",
                "subject": "asunto",
                "message": "mensaje"
            }
        },
        "pages": {
            "index": {
                "title": "Páginas",
                "fields": {
                    "key": "Clave",
                    "title": "Titulo",
                    "ht_title": "Ht titulo",
                    "url": "Url",
                    "view_page": "ir a la web",
                    "actions": "Acciones"
                },
                "buttons": {
                    "edit": "editar",
                    "delete": "borrar"
                }
            },
            "create": {
                "title": "Crear Pagina",
                "fields": {
                    "key": "Clave",
                    "name": "Nombre",
                    "model": "Modelo",
                    "actions": "Acciones"
                },
                "buttons": {
                    "create": "crear",
                    "cancel": "cancelar"
                },
                "success_msg": "Pagina creado correctament!"
            },
            "edit": {
                "title": "Modificar Pagina - {page}",
                "fields": {
                    "key": "Clave",
                    "name": "Nombre",
                    "model": "Modelo",
                    "actions": "Acciones"
                },
                "buttons": {
                    "save": "guardar",
                    "cancel": "cancelar"
                },
                "success_msg": "¡Pagina modificado correctamente!"
            },
            "delete": {
                "title": "Eliminar Pagina - {page}",
                "info_msg": "¿Estás seguro de que deseas eliminar la {page} ?",
                "success_msg": "¡Pagina modificado correctamente!"
            }
        }
    }
}
