webpackJsonp([11],{

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__401_vue_vue_type_template_id_51c6ea73_scoped_true___ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__401_vue_vue_type_script_lang_js___ = __webpack_require__(489);
/* empty harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__401_vue_vue_type_style_index_0_id_51c6ea73_rel_stylesheet_2Fscss_lang_scss_scoped_true___ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__ = __webpack_require__(8);






/* normalize component */

var component = Object(__WEBPACK_IMPORTED_MODULE_3__node_modules_vue_loader_lib_runtime_componentNormalizer_js__["a" /* default */])(
  __WEBPACK_IMPORTED_MODULE_1__401_vue_vue_type_script_lang_js___["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_0__401_vue_vue_type_template_id_51c6ea73_scoped_true___["a" /* render */],
  __WEBPACK_IMPORTED_MODULE_0__401_vue_vue_type_template_id_51c6ea73_scoped_true___["b" /* staticRenderFns */],
  false,
  null,
  "51c6ea73",
  null
  
)

/* hot reload */
if (false) {
  var api = require("/var/www/html/latevaweb-testing/ltw-backend-test1/node_modules/vue-hot-reload-api/dist/index.js")
  api.install(require('vue'))
  if (api.compatible) {
    module.hot.accept()
    if (!module.hot.data) {
      api.createRecord('51c6ea73', component.options)
    } else {
      api.reload('51c6ea73', component.options)
    }
    module.hot.accept("./401.vue?vue&type=template&id=51c6ea73&scoped=true&", function () {
      api.rerender('51c6ea73', {
        render: render,
        staticRenderFns: staticRenderFns
      })
    })
  }
}
component.options.__file = "resources/js/backend/views/errorPage/401.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_script_lang_js___ = __webpack_require__(490);
/* unused harmony namespace reexport */
 /* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_script_lang_js___["a" /* default */]); 

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_401_images_401_gif__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_401_images_401_gif___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_401_images_401_gif__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'Page401',
  data: function data() {
    return {
      errGif: __WEBPACK_IMPORTED_MODULE_0__assets_401_images_401_gif___default.a + '?' + +new Date(),
      ewizardClap: 'https://wpimg.wallstcn.com/007ef517-bafd-4066-aae4-6883632d9646',
      dialogVisible: false
    };
  },

  methods: {
    back: function back() {
      if (this.$route.query.noGoBack) {
        this.$router.push({ path: '/dashboard' });
      } else {
        this.$router.go(-1);
      }
    }
  }
});

/***/ }),

/***/ 491:
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(547);

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(10)(content, options);

if(content.locals) module.exports = content.locals;

if(false) {
	module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./401.vue?vue&type=style&index=0&id=51c6ea73&rel=stylesheet%2Fscss&lang=scss&scoped=true&", function() {
		var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./401.vue?vue&type=style&index=0&id=51c6ea73&rel=stylesheet%2Fscss&lang=scss&scoped=true&");

		if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_template_id_51c6ea73_scoped_true___ = __webpack_require__(544);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_template_id_51c6ea73_scoped_true___["a"]; });
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_template_id_51c6ea73_scoped_true___["b"]; });


/***/ }),

/***/ 544:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "errPage-container" },
    [
      _c(
        "el-button",
        {
          staticClass: "pan-back-btn",
          attrs: { icon: "arrow-left" },
          on: { click: _vm.back }
        },
        [_vm._v("返回")]
      ),
      _vm._v(" "),
      _c(
        "el-row",
        [
          _c("el-col", { attrs: { span: 12 } }, [
            _c("h1", { staticClass: "text-jumbo text-ginormous" }, [
              _vm._v("Oops!")
            ]),
            _vm._v("\n      gif来源"),
            _c(
              "a",
              { attrs: { href: "https://zh.airbnb.com/", target: "_blank" } },
              [_vm._v("airbnb")]
            ),
            _vm._v(" 页面\n      "),
            _c("h2", [_vm._v("你没有权限去该页面")]),
            _vm._v(" "),
            _c("h6", [_vm._v("如有不满请联系你领导")]),
            _vm._v(" "),
            _c("ul", { staticClass: "list-unstyled" }, [
              _c("li", [_vm._v("或者你可以去:")]),
              _vm._v(" "),
              _c(
                "li",
                { staticClass: "link-type" },
                [
                  _c("router-link", { attrs: { to: "/dashboard" } }, [
                    _vm._v("回首页")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("li", { staticClass: "link-type" }, [
                _c("a", { attrs: { href: "https://www.taobao.com/" } }, [
                  _vm._v("随便看看")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c(
                  "a",
                  {
                    attrs: { href: "#" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        _vm.dialogVisible = true
                      }
                    }
                  },
                  [_vm._v("点我看图")]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("el-col", { attrs: { span: 12 } }, [
            _c("img", {
              attrs: {
                src: _vm.errGif,
                width: "313",
                height: "428",
                alt: "Girl has dropped her ice cream."
              }
            })
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "el-dialog",
        {
          attrs: { visible: _vm.dialogVisible, title: "随便看" },
          on: {
            "update:visible": function($event) {
              _vm.dialogVisible = $event
            }
          }
        },
        [_c("img", { staticClass: "pan-img", attrs: { src: _vm.ewizardClap } })]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 545:
/***/ (function(module, exports) {

module.exports = "/images/401.gif?089007e721e1f22809c0313b670a36f1";

/***/ }),

/***/ 546:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_style_index_0_id_51c6ea73_rel_stylesheet_2Fscss_lang_scss_scoped_true___ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_style_index_0_id_51c6ea73_rel_stylesheet_2Fscss_lang_scss_scoped_true____default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_style_index_0_id_51c6ea73_rel_stylesheet_2Fscss_lang_scss_scoped_true___);
/* unused harmony reexport namespace */
 /* unused harmony default export */ var _unused_webpack_default_export = (__WEBPACK_IMPORTED_MODULE_0__node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_sass_loader_lib_loader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_401_vue_vue_type_style_index_0_id_51c6ea73_rel_stylesheet_2Fscss_lang_scss_scoped_true____default.a); 

/***/ }),

/***/ 547:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)(false);
// imports


// module
exports.push([module.i, ".errPage-container[data-v-51c6ea73] {\n  width: 800px;\n  max-width: 100%;\n  margin: 100px auto;\n}\n.errPage-container .pan-back-btn[data-v-51c6ea73] {\n    background: #008489;\n    color: #fff;\n    border: none !important;\n}\n.errPage-container .pan-gif[data-v-51c6ea73] {\n    margin: 0 auto;\n    display: block;\n}\n.errPage-container .pan-img[data-v-51c6ea73] {\n    display: block;\n    margin: 0 auto;\n    width: 100%;\n}\n.errPage-container .text-jumbo[data-v-51c6ea73] {\n    font-size: 60px;\n    font-weight: 700;\n    color: #484848;\n}\n.errPage-container .list-unstyled[data-v-51c6ea73] {\n    font-size: 14px;\n}\n.errPage-container .list-unstyled li[data-v-51c6ea73] {\n      padding-bottom: 5px;\n}\n.errPage-container .list-unstyled a[data-v-51c6ea73] {\n      color: #008489;\n      text-decoration: none;\n}\n.errPage-container .list-unstyled a[data-v-51c6ea73]:hover {\n        text-decoration: underline;\n}\n", ""]);

// exports


/***/ })

});